\chapter{Analýza}

\section{Základní otázky}

\subsection{Univerzálnost}
Systém je sice určen pro zpracování automatů a gramatik, ale je vhodné, aby
návrh systému dokázal pojmout i další formální jazyky. Například speciální
automaty, regulární výrazy a jejich různé dialekty či dokonce Petriho sítě a
L-systémy. Obecně by systém měl být připraven na implementaci libovolného typu
dat a nad nimi libovolných algoritmů.

\subsection{Modularita}
Aby systém mohl nad formálními jazyky nějaké algoritmy provádět, musí těmito
algoritmy disponovat. Mohou být zabudované přímo do programu jak je tomu běžně
zvykem u většiny ``blackbox'' systémů. Takový přístup přináší lehce vyšší výkon
(systém se nezdržuje při interakci s okolím, které poskytuje potřebnou
funkcionalitu) a umožňuje konzistentní distribuci a existenci systému. Na druhou
stranu je však takový systém omezen pevně stanoveným rozsahem schopností a
neumožňuje implementovat snadno další funkcionalitu bez nutnosti změn v
základech systému. Modularita umožňuje uživatelům relativně snadný zásah do
spektra schopností systému a umožňuje okamžitě přidávat algoritmy, které jsou
třeba aktuální součástí výzkumu a tedy nemohou být hlavním správcem systému
zahrnuty v základní distribuci.

Modularita systému přeměňuje celou jeho architekturu z typu ``kafemlýnek'' na typ
``platforma''. Systém se tak stává programovou základnou pro implementaci
libovolných algoritmů.

\subsection{Přenositelnost}
Přenositelnost systému na jiné platformy spočívá ve využití standardizovaných
technologií a postupů. Nežádoucí je například využití platformně závislých
knihoven. Není však možné implementovat systém tak, aby fungoval stejně na všech
existujících platformách, proto jsem zvolil jednu konkrétní platformu - systémy
splňující normu POSIX \cite{posix}. Tedy všechny systémy GNU/Linux a všechny
UNIX operační systémy, jako je například Solaris a rodina systémů BSD. V
akademické sféře je použití takových systémů zcela běžné a právě na tuto oblast
je produkt mířen. V případě potřeby přenesení produktu na jiné operační systémy
je v sekci Implementace popsán základní postup. Zvolená licence (MIT) a umístění
zdrojových kódů na Internetu umožňuje bezproblémové upravování a redistribuci
dle libosti.

\subsection{Jednoduchost}
Výše zmíněné vlastnosti systému mohou vést k přílišné komplikovanosti a
neprůhlednosti jeho návrhu a implementace. Je potřeba tedy návrh systému sice k
těmto vlastnostem vést, ale pokud možno nejjednodušší cestou. Není na škodu,
když systém bude ``hloupější'' nebo se nebude vyhýbat všem potencionálním
propastem, protože vytvořit takový systém ``na zelené louce'', dokonalý na první
pokus, je čistá softwarová naivita. Navíc, pokud bude návrh extrémně špatný,
jediná nad ním proveditelná akce je vylepšit ho. A s takovými iteracemi
vylepšení může návrh a implementace systému nakonec dokonvergovat (nebo se spíše
limitně blížit) k dokonalosti. Otázkou ale zůstává, kolik asi takových iterací
je potřeba.

\section{Architektura systému}
Při návrhu architektury systému je nutné nejprve rozdělit problém na menší
celky (divide and conquer) pro zjednodušení implementace a usnadnění případných
budoucích majoritních změn systému. Celý systém se dá popsat jako černá skřínka
(black box) se vstupem a výstupem. Vstupy jsou datové soubory a konfigurace
programu, výstupem pak transformované datové soubory nebo výpisy programu. Pro
definování architektury je tedy potřeba stanovit část, která bude přijímat data
a zpracovávat konfiguraci, část která bude rozhodovat o řízení programu a části
určené pro samotné zpracování dat. Následující tři návrhy popisují mou snahu
takovou architekturu definovat, přičemž první dva jsou neúspěšné pokusy a
poslední reprezentuje implementované řešení.

\subsection{První návrh}
První návrh architektury (obrázek \ref{fig:arch1}) spočíval v rozdělení systému
na jádro (core), které řídí celý program a přijímá vstup. Jádro spolupracuje s
uživatelským rozhraním (interface) a~pro zpracování přepínačů z příkazové řádky
používá OptionsHandler.  Vstupní data jsou parsována pomocí lexikálního
analyzátoru a syntaktického analyzátoru v parseru do vnitřních datových struktur
DataHandleru. Tato data jsou pak předávána správci modulů, který spolupracuje s
algoritmickými moduly.  Výsledek je předán ResultHandleru a ten vyprodukuje
výstup.

Jako zbytečné se ukázalo vytvářet speciální vrstvu (library) k algoritmickým
modulům. Za cenu lehce rozsáhlejší implementace správce modulů mohla být
vypuštěna. Odloučené rozhraní od jádra má své výhody, jako například snazší
lokalizace, či jednodušší záměna rozhraní. Do systému zabudovaný ResultHandler
neumožňuje přidávat nové způsoby exportu transformovaných vstupů. Dále je
potřeba jemněji definovat strukturu DataHandleru a navrhnout proces parsování
vstupních dat.
\begin{figure}[ht]
\begin{center}
\includegraphics[width=150mm]{figures/archs/arch_01.pdf}
\caption{První architektura}
\label{fig:arch1}
\end{center}
\end{figure}

\subsection{Druhý návrh}
Ve druhém návrhu (obrázek \ref{fig:arch2}) byla odstraněna mezivrstva ke
knihovnám a veškeré řízení bylo zabudováno do jádra. Tím se návrh značně
zjednodušil, ale ve výsledku by ze se systému stal monolitický kus kódu, se
kterým by se dál špatně pracovalo (z vývojářského hlediska). Zajímavý nápad této
architektury tkví v integraci importu i exportu datových formátů do
DataHandleru. A to buď jako lehce redundantní řešení - každý datový formát by
měl svoje vlastní vstupní a výstupní metody (tedy vlastní lexan, parser, vlastní
datové struktury, vlastní exportní metody atp.), což by usnadnilo snazší
případnou modularitu datových formátů a nebo jako jedna skupina importních a
exportních algoritmů společná pro všechny datové formáty, což sice systém
zmenší, ale návrh i systém to příliš zkomplikuje.
\begin{figure}[ht]
\begin{center}
\includegraphics[width=150mm]{figures/archs/arch_02.pdf}
\caption{Druhá architektura}
\label{fig:arch2}
\end{center}
\end{figure}

\subsection{Třetí návrh}
Třetí návrh (obrázek \ref{fig:arch3}) má stejně jako předchozí návrhy jádro, jen
je přejmenované na Controller. Zpracování argumentů probíhá ve specializovaném
oddílu programu.  Controller při přijetí vstupů a po dohodě s OptionsHandlerem
vyšle požadavek na načtení vstupních datových souborů parseru. Ten pro každý
soubor přečte záhlaví souboru udávající jeho typ a podle této informace spustí
příslušný Subparser.  Ten ze zbytku obsahu vstupního souboru vygeneruje datový
typ (automat, gramatika nebo PlainRegex) a tento datový typ vloží do Containeru.
Parser obsahuje zabudovaný lexikální analyzátor, který Subparserům předává. Po
dokončení transformace na vnitřní datové struktury Controller požádá
ActionManager o spuštění algoritmického modulu, kterému je předána celá skupina
kontejnerů obsahující na vnitřní formu transformovaná vstupní data. Po dokončení
práce algoritmického modulu je pak skupina kontejnerů předána ExportManageru,
který se postará o vyprodukování výstupu.

Datové struktury popisující formální jazyky jsou tak sice součástí systému, ale
vzhledem k tomu, že se jich běžně nepoužívá až takové množství, nemusí být
odtrženy jako moduly. Při potřebě přidat nový datový typ, například zásobníkový
automat, stačí definovat a implementovat novou datovou strukturu
``PushdownAutomata'' (například), napsat pro ni Subparser a zaregistrovat nový
typ do parseru a kontejneru. Bohužel s touto změnou pak souvisí nutnost
doprogramovat způsob exportu tohoto typu do exportních modulů. V implementaci
však všechny tyto změny nejsou nijak náročné (mnohem těžší je vymyslet rozhraní
nové datové struktury).

Odtržení algoritmů pro export výsledků systému do modulů přináší některé
komplikace, ale ve výsledku umožňuje snadno doprogramovávat různé typy výstupů,
jako například převod transformovaných dat v systému do jazyka DOT systému
Graphviz nebo převod na nějaký XML formát dat bez nutnosti zásahu do systému.

Řešení závislostí mezi datovými formáty a (algoritmickými i exportními) moduly
je řešeno pomocí informací, poskytovanými kontejnerem. Ten ``ví'', jakou datovou
strukturu obsahuje a moduly by tuto informaci měly vždy prověřovat (záleží však
pouze na programátorovi modulu).
\begin{figure}[ht]
\begin{center}
\includegraphics[width=150mm]{figures/archs/arch_03.pdf}
\caption{Třetí architektura}
\label{fig:arch3}
\end{center}
\end{figure}


\section{Uživatelské rozhraní}
V dnešní době existuje mnoho různých druhů uživatelských rozhraní, které
umožňují uživateli interagovat s programem. Pokusím se stručně rozebrat některé
nejznámější druhy a jejich případný dopad na použitelnost systému.

\subsection{Grafické uživatelské rozhraní}
Moderním trendem uživatelských aplikací je grafické uživatelské rozhraní (GUI).
Mezi jeho výhody patří především vyšší intuitivnost a v případě dobrého navržení
i jednoduchost používání celé aplikace. Na druhou stranu však omezuje použití na
více platformách a nelze jej použít na systémech bez grafického uživatelského
rozhraní (typicky servery). Také dávkové zpracování nebo automatická spolupráce
s ostatními programy je omezená a kód je náročnější na implementaci i správu.

\subsection{Příkazové rozhraní}
Příkazové rozhraní (CLI) můžeme najít typicky v příkazových interpreterech a
některých skriptovacích jazycích. Například v Bourne Again Shell (BASH), C Shell
(CSH) nebo Python, či nástroji pro tvorbu grafů Gnuplot. Toto rozhraní umožňuje
uživateli rozmělnit práci na sled menších kroků, které mohou být zaznamenány
jako dávkový soubor (skript). Také klade velmi nízké nároky na použitý hardware
a software. Toto rozhraní jsem nezvolil, protože jsem chtěl systém sestavit jako
blackbox s velmi jednoduchým návrhem. V případě vylepšování návrhu a přepisování
systému by bylo vhodné použít právě tento druh rozhraní a koncipovat systém více
jako programovací jazyk, než jako utilitu.

\subsection{Parametrické rozhraní}
Parametrické rozhraní je velmi rozšířené v UN*Xovém světě. Vyznačuje se nulovou
interakcí s uživatelem po spuštění a v době běhu programu. Program přijme pouze
parametry (argumenty) na příkazové řádce příkazového interpretru a podle těchto
parametrů koncipuje své řízení. Parametrické rozhraní je velmi vhodné pro
software, který vykonává jeden druh úlohy, kde se mění jen vstupní data nebo
parametry vykonávaného algoritmu.

Téměř každou aplikaci v UN*Xu lze konfigurovat při startu pomocí parametrů
na příkazové řádce. Velká část aplikací má kromě parametrického rozhraní ještě
dále CLI, GUI, TUI či jiné druhy rozhraní, přičemž parametry slouží pro prvotní
konfiguraci chování programu.

Parametrické rozhraní má stejně jako CLI velmi nízké nároky na použitý hardware
i~software a je velice dobře přenositelné. Nevýhodou může být nízká intuitivnost
- uživatel je nucen si přečíst dokumentaci k programu dříve, než s ním začne
pracovat. Toto rozhraní jsem nakonec vybral, protože je velmi jednoduché a
odpovídá původnímu navrhovanému stylu systému - blackbox. V případě dalších
prací na vylepšování systému bych nyní zvolil spíše příkazové rozhraní (CLI),
koncipoval systém více jako programovací jazyk a~platformu než jako
jednoúčelovou aplikaci.

\subsection{Webové rozhraní}
Webové uživatelské rozhraní (WUI) je díky rozpínajícímu se Internetu často
používaným uživatelským rozhraním pro mnoho různých aplikací. WUI je často
velice intuitivní a~snadno přenositelné. Propojením značkovacího jazyka (X)HTML
a~skriptovacího jazyka PHP můžeme snadno sestavit webovou nadstavbu našemu
systému. Například webový formulář pro zadání automatu by uživatelem zadaná data
poslal do aplikace na zpracování automatů, který by provedl požadovanou akci,
výsledek exportoval v jazyce DOT a~tento výsledek by byl přesměrován do systému
Graphviz pro vygenerování bitmapového obrázku, který by byl poté zobrazen zpět
na webové stránce uživateli jako výsledek.

\section{Programovací jazyk}
Před implementací je potřeba zvolit programovací jazyk, ve kterém bude systém
napsán. Dnes je k dispozici velké množství krásných jazyků, ale bylo potřeba
vybrat jen jeden. Sice je možnost systém sestavit z několika jazyků, což sice
nese svoje výhody (rychlejší vývoj, efektivnější konstrukce, \ldots), ale také
množství nevýhod (náročnější kontrola kódu, nutnost vytváření spolupráce jazyků,
\ldots). Jazyky obecně preferované jsou většinou vysokoúrovňové imperativní a
objektové.  Skriptovacími jazyky (perl atp.) by se tvorba a udržování kódu
velice zrychlila.  Kdybych nezvolil kompilované jazyky, skriptovací by byly
pravděpodobně také dostatečně vhodné. Také bylo možné využít jazyků
funkcionálních. Myslím, že právě pro tento systém by funkcionální jazyky
dokázaly předvést své schopnosti velmi působivě, ale malá rozšířenost je z
výběru vyřadila. Nakonec jsem tedy vybíral z klasické dvojice C/C++ a Java,
která je v mém akademickém prostředí nejvíce podporována.

\subsection{Java}
Programovací jazyk Java je dnes rozšířený jazyk. V akademickém světě zastupuje
nepřehlédnutelnou pozici díky dobré přenositelnosti a relativně kvalitnímu
návrhu. Zvolit programovací jazyk Java pro implementaci systému pro zpracování
automatů a gramatik by bylo možná vhodné, ale výsledný produkt by byl závislý na
virtuálním stroji pro interpretaci javového bytecode. Tento virtuální stroj je
sice běžně obsažen na většině uživatelských stanic, ale na serverech a méně
výkonných počítačích nemusí být vždy přítomen. Navíc nároky takového běhového
prostředí jsou znatelné. Co je také nepříjemným aspektem použití programovacího
jazyka Java v případě programování mého systému, je rychlost spuštění a vypnutí
samotného programu a rychlost běhu algoritmů. V případě akademického prostředí
je to relativně nepodstatný fakt, ale v případě potřeby dávkového zpracování
velkého množství dat nejsou takové vlastnosti uspokojující. Javu jsem nakonec
nezvolil z osobních důvodů.

\subsection{C++}
Většinu nedostatků Javy řeší použití jazyka jako je C++ \cite{cppr}.
Zkompilovaný program je nenáročný na systémové prostředky, nemá znatelné
závislosti nutné pro svůj běh a pokud je dobře napsaný, dá se snadno přenášet na
jiné platformy. Program psaný v C++ vyniká například oproti jazyku Java
rychlostí startu a ukončení a i běhu algoritmů. Na druhou stranu je mnohem
náročnější na zkušenosti programátora a~umožňuje psát velmi nestabilní
a~nebezpečný kód. Také vyjadřovací schopnosti tohoto jazyka vedou k rozsáhlejším
zdrojovým kódům než je tomu u ostatních jazyků, ale umožňuje velmi zajímavé
konstrukty a dává velkou svobodu při tvorbě celého systému. Volba C++ je myslím
pro tento systém volbou vhodnou.


