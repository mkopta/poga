/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Data structures holding automaton                                 }}}1*/

#include <string>
#include <set>
#include <algorithm>
#include <functional>
#include "./poga.h"
#include "./data_types.h"

using std::string;
using std::greater;
using std::set;
using data_types::Automaton;
using data_types::automaton_ns::State;
using data_types::automaton_ns::Transition;
using data_types::automaton_ns::HalfTransition;

/* CLASS State */
/* private:
     bool   is_final;
     bool   is_initial;
     string name;
*****************************************************************************/

State::State(string name, bool is_final, bool is_initial) {
  this->name = name;
  this->is_final = is_final;
  this->is_initial = is_initial;
}

State::State(const State &st) {
  this->name = st.name;
  this->is_final = st.is_final;
  this->is_initial = st.is_initial;
}

State::~State() {}

bool State::isFinal() const {
  return this->is_final;
}

bool State::isInitial() const {
  return this->is_initial;
}

string State::getName() const {
  return this->name;
}

void State::setIsInit(bool is) {
  this->is_initial = is;
}

void State::setIsFinal(bool is) {
  this->is_final = is;
}

void State::setName(string _name) {
  this->name = _name;
}

bool State::operator<(const State &st) const {
  if (st.name == this->name) {
    return false;
  } else if (max(st.name, this->name, greater<string>()) == st.name) {
    return false;
  } else {
    return true;
  }
}

bool State::operator==(const State &st) const {
  return st.name == this->name;
}

bool State::operator!=(const State &st) const {
  return st.name != this->name;
}

State State::operator=(const State &copy) {
  is_initial = copy.is_initial;
  is_final = copy.is_final;
  name = copy.name;
  return copy;
}


/* CLASS Transition */
/* private:
     string symb;
     State  dest;
*****************************************************************************/
HalfTransition::HalfTransition(string _symb, State _dest)
  :dest(_dest), symb(_symb)
{}

HalfTransition::~HalfTransition() {}

State HalfTransition::getDest() const {
  return this->dest;
}

string HalfTransition::getSymb() const {
  return this->symb;
}

bool HalfTransition::isDestFinal() const {
  return (this->dest).isFinal();
}

void HalfTransition::setDest(State state) {
  this->dest = state;
}

void HalfTransition::setSymb(string symb) {
  this->symb = symb;
}

bool HalfTransition::symbEquals(string *symb) const {
  return *symb == this->symb;
}

bool HalfTransition::destEquals(State *state) const {
  return this->dest == *state;
}

bool HalfTransition::operator<(const HalfTransition &ht) const {
  if (this->operator == (ht)) {
    return false;
  }
  if (max(ht.symb, this->symb, greater<string>()) == ht.symb) {
    if (this->dest < ht.dest) {
      return true;
    } else {
      return true;
    }
  } else if (max(ht.symb, this->symb, greater<string>()) == this->symb) {
    if (ht.dest < this->dest) {
      return false;
    } else {
      return true;
    }
  }
  return false;
}

bool HalfTransition::operator==(const HalfTransition &ht) const {
  if (this->symb != ht.symb) return false;
  if (this->dest != ht.dest) return false;
  return true;
}

bool HalfTransition::operator!=(const HalfTransition &ht) const {
  return !(this->operator == (ht));
}

/* CLASS Transition */
/* private:
     State  src;
     string symb;
     State  dest;
*****************************************************************************/

Transition::Transition(State _src, string _symb, State _dest)
  :src(_src), symb(_symb), dest(_dest) {}

Transition::~Transition() {}

State Transition::getSrc() const {
  return this->src;
}

string Transition::getSymb() const {
  return this->symb;
}

State Transition::getDest() const {
  return this->dest;
}

bool Transition::isDestFinal() const {
  return (this->dest).isFinal();
}

bool Transition::isSrcInit() const {
  return (this->src).isInitial();
}

bool Transition::symbEquals(string *symbol) {
  return this->symb == *symbol;
}

bool Transition::srcEquals(State *state) {
  return this->src == *state;
}

bool Transition::destEquals(State *state) {
  return this->dest == *state;
}

bool Transition::equals(Transition *trans) {
  if (!trans->srcEquals (&this->src )) return false;
  if (!trans->symbEquals(&this->symb)) return false;
  if (!trans->destEquals(&this->dest)) return false;
  return true;
}

bool Transition::operator<(const Transition &tr) const {
  if (this->src < tr.src) return true;
  if (max(tr.symb, this->symb, greater<string>()) == tr.symb) {
    return true;
  }
  if (this->dest < tr.dest) return true;
  return false;
}

bool Transition::operator==(const Transition &tr) const {
  if (this->src  != tr.src ) return false;
  if (this->symb != tr.symb) return false;
  if (this->dest != tr.dest) return false;
  return true;
}

bool Transition::operator!=(const Transition &tr) const {
  return !(this->operator == (tr));
}

/* CLASS Automaton */
/* private:
     set<State>         discreteStates;
     set<string>        unusedSymbols;
     set<Transition>    transitions;
     string             comment;
     set<State>         init;
*****************************************************************************/

Automaton::Automaton() {}

Automaton::Automaton(const set<State> &_states, const set<string> &_symbols) {
  this->discreteStates  = _states;
  this->unusedSymbols   = _symbols;
  (this->transitions).clear();
}

Automaton::~Automaton() {}

void Automaton::addTransition(const State _src, const string _symbol,
    const State _dest) {
  set<State>::iterator  it1;
  set<string>::iterator it2;

  it1 = discreteStates.find(_src);
  if (it1 != discreteStates.end())
    discreteStates.erase(it1);

  it1 = discreteStates.find(_dest);
  if (it1 != discreteStates.end())
    discreteStates.erase(it1);

  it2 = unusedSymbols.find(_symbol);
  if (it2 != unusedSymbols.end())
    unusedSymbols.erase(it2);

  Transition tr(_src, _symbol, _dest);
  (this->transitions).insert(tr);
}

void
Automaton::addTransition(const Transition &  _t) {
  this->addTransition(_t.getSrc(), _t.getSymb(), _t.getDest());
}

void
Automaton::addState(const State & _state) {
  this->discreteStates.insert(_state);
}

void
Automaton::addSymbol(const string & _symbol) {
  this->unusedSymbols.insert(_symbol);
}

void Automaton::setComment(string _comment) {
  this->comment = _comment;
}

string
Automaton::getComment() const {
  return this->comment;
}

set<string>
Automaton::getUnusedSymb() const {
  return this->unusedSymbols;
}

set<State>
Automaton::getDiscreteStates() const {
  return this->discreteStates;
}

set<Transition>
Automaton::getTransitions() const {
  return this->transitions;
}

set<string>
Automaton::getAlphabet() const {
  set<string> alpha = this->unusedSymbols;
  set<Transition>::iterator it_tr = this->transitions.begin();
  for (; it_tr != this->transitions.end(); it_tr ++)
    alpha.insert(it_tr->getSymb());
  return (alpha);
}

set<string>
Automaton::getSymbols() const {
  return this->getAlphabet();
}

set<State>
Automaton::getStates(void) const {
  set<State> states = this->discreteStates;
  set<Transition>::iterator it_tr = this->transitions.begin();
  for (; it_tr != this->transitions.end(); it_tr++) {
    states.insert(it_tr->getSrc());
    states.insert(it_tr->getDest());
  }
  return states;
}

void
Automaton::setInit(const set<State> & _init) {
  this->init = _init;
}

set<State>
Automaton::getInit(void) const {
  return (this->init);
}

set<HalfTransition> Automaton::expand(const State &src) const {
  set<HalfTransition> nieghbours;
  set<Transition>::iterator it;
  for (it = this->transitions.begin(); it != this->transitions.end(); it++) {
    if (it->getSrc() == src) {
      string symb = it->getSymb();
      State  dest = it->getDest();
      HalfTransition ht(symb, dest);
      nieghbours.insert(ht);
    }
  }
  return (nieghbours);
}

set<State> Automaton::cross(const State & src, const string & symbol) const {
  set<State> dest;
  set<Transition>::iterator it;
  for (it = this->transitions.begin(); it != this->transitions.end(); it++) {
    if (it->getSrc() == src && it->getSymb() == symbol) {
      dest.insert(it->getDest());
    }
  }
  return dest;
}

void Automaton::clear() {
  this->transitions.clear();
  this->unusedSymbols.clear();
  this->discreteStates.clear();
  this->comment.clear();
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
