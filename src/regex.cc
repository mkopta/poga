/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Data structure holding regular expressions                        }}}1*/

#include <cassert>
#include <vector>
#include <string>
#include "./data_types.h"

using std::vector;
using std::string;
using data_types::PlainRegex;

/* CLASS PlainRegex */
/* private:
   vector<char>  expression;
   unsigned int  mark      ;
 *****************************************************************************/

PlainRegex::PlainRegex(): mark(0) {}

PlainRegex::PlainRegex(vector<char> _expression)
  :mark(0), expression(_expression) {}

PlainRegex::PlainRegex(string expression):mark(0) {
  for (unsigned int i = 0; i < expression.length(); i ++)
    (this->expression).push_back(static_cast<char>(expression[i]));
}

PlainRegex::~PlainRegex() {}

void PlainRegex::append(char character) {
  (this->expression).push_back(character);
}

void PlainRegex::setExpression(vector<char> expression) {
  this->expression = expression;
}

void PlainRegex::setExpression(string expression) {
  (this->expression).clear();
  for (unsigned int i = 0; i < expression.length(); i ++)
    (this->expression).push_back(static_cast<char>(expression[i]));
}

vector<char> PlainRegex::getExpression() const {
  return this->expression;
}

int PlainRegex::length() const {
  return static_cast<int>((this->expression).size());
}

string PlainRegex::toString() const {
  if ((this->expression).size() == 0)
    return string("");

  string expression_as_string("");
  for (unsigned int i = 0; i < (this->expression).size(); i++) {
    expression_as_string.push_back((this->expression)[i]);
  }
  return expression_as_string;
}

int PlainRegex::setMark(unsigned int position) {
  if (position >=(this->expression).size()) {
    return 1;
  } else {
    this->mark = position;
  }
  return 0;
}

char PlainRegex::getNextChar() {
  if (mark >= (this->expression).size()) {
    this->mark = 0;
    return 0;
  } else {
    char c = (this->expression)[mark];
    this->mark++;
    return c;
  }
}

void PlainRegex::setComment(string _comment) {
  this->comment = _comment;
}

string PlainRegex::getComment() const {
  return this->comment;
}

char
PlainRegex::operator[](unsigned int position) const {
  assert(position >= (this->expression).size());
  return (this->expression)[position];
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
