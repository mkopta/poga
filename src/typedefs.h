/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Typedefs for poga                                                 }}}1*/

#ifndef SRC_TYPEDEFS_H_
#define SRC_TYPEDEFS_H_

#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>

class Container;

namespace data_types {
  namespace grammar_ns {
    class Element;
    class Rule;
  }
  namespace automaton_ns {
    class State;
    class Transition;
    class HalfTransition;
  }
  class Automaton;
  class Grammar;
  class PlainRegex;
}

typedef data_types::automaton_ns::State                    ST;
typedef data_types::automaton_ns::Transition               TR;
typedef data_types::grammar_ns::Element                    EL;
typedef data_types::grammar_ns::Rule                       RU;
typedef std::map<std::string, std::string>                 MAP_STR_STR;
typedef std::set<data_types::automaton_ns::HalfTransition> SET_HT;
typedef std::set<data_types::automaton_ns::State>          SET_ST;
typedef std::set<data_types::automaton_ns::Transition>     SET_TR;
typedef std::set<data_types::grammar_ns::Element>          SET_EL;
typedef std::set<data_types::grammar_ns::Rule>             SET_RU;
typedef std::set<std::string>                              SET_STR;
typedef std::string                                        STR;
typedef std::stringstream                                  SSTR;
typedef std::vector<Container>                             VEC_CNT;
typedef std::vector<char>                                  VEC_CH;
typedef std::vector<data_types::automaton_ns::State>       VEC_ST;
typedef std::vector<data_types::grammar_ns::Element>       VEC_EL;
typedef std::vector<data_types::grammar_ns::Rule>          VEC_RU;
typedef std::vector<std::string>                           VEC_STR;
typedef std::vector<unsigned int>                          VEC_UI;
typedef unsigned int                                       UI;

#endif  // SRC_TYPEDEFS_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
