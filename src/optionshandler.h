/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Optionshandler header file for poga                               }}}1*/

#ifndef SRC_OPTIONSHANDLER_H_
#define SRC_OPTIONSHANDLER_H_

#include "typedefs.h"

class OptionsHandler {
  private:
    int helpFlag;
    int usageFlag;
    int versionFlag;
    int outputFileFlag;
    STR actionName;
    VEC_STR inputFiles;
    STR outputFilename;
    STR outputFormat;
    STR message;
    int argc;
    char** argv;

  public:
    OptionsHandler(int argc, char **argv);
    virtual ~OptionsHandler();
    virtual int read();
    virtual int isHelpFlag();
    virtual int isUsageFlag();
    virtual int isVersionFlag();
    virtual int isOutputFileFlag();
    virtual VEC_STR getInputFilenames();
    virtual STR getOutputFilename();
    virtual STR getMessage();
    virtual STR getActionName();
    virtual STR getOutputFormat();
};

#endif  // SRC_OPTIONSHANDLER_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
