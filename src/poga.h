/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Poga header file for poga                                         }}}1*/

#ifndef SRC_POGA_H_
#define SRC_POGA_H

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;

#ifndef VERSION
#define VERSION __TIMESTAMP__
#endif

#ifdef DEBUG
#define DEBUG 1
#define PRINT_DEBUG(s) cerr << __FILE__ << \
  " [" << __LINE__ << "]: " << s << endl
#else
#define DEBUG 0
#define PRINT_DEBUG(s)
#endif

#define PRINT_ERROR(s) cerr << s << endl

#ifdef WIN32
#error "Poga is not ported to Windows (yet) !"
#elif defined(__linux__) || defined(__unix__)
#define POGA4LIN 1
#else
#error "Cannot recognize your platform! Contact the developer please."
#endif

#endif  // SRC_POGA_H

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
