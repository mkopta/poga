/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Commandline handler for poga                                      }}}1*/

#include <getopt.h>
#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>
#include "./optionshandler.h"
#include "./poga.h"

using std::vector;
using std::string;
using std::cout;
using std::cerr;
using std::endl;

OptionsHandler::OptionsHandler(int argc, char **argv):helpFlag(0),
  usageFlag(0), versionFlag(0), outputFileFlag(0), actionName(""),
  outputFilename(""), outputFormat(""), message(""), argc(argc),
  argv(argv) {}

OptionsHandler::~OptionsHandler() {}

/* Reads cmdline and set flags and fills data structures */
int OptionsHandler::read() {
  /* at least one option */
  if (this->argc < 2) {
    this->usageFlag = 1;
    return 1;
  }

  /*
   * Processing of switches
   ***************************/
  /* going thru commandline */
  char c;
  while (1) {
    static struct option long_options[] = {
      {"help", no_argument, &this->helpFlag, 1},
      {"usage", no_argument, &this->usageFlag, 1},
      {"version", no_argument, &this->versionFlag, 1},
      {"action", required_argument, 0, 'a'},
      {"outputfile",  required_argument, 0, 'o'},
      {"output-format", required_argument, 0, 'f'},
      {0, 0, 0, 0}
    };

    int option_index = 0;

    /* get next switch */
    c = getopt_long(this->argc, this->argv, "huva:f:o:", long_options,
        &option_index);

    /* if we are at the end of switches */
    if (c == -1) break;

    /* evaluating of switch */
    switch (c) {
      case 0:
        if (long_options[option_index].flag != 0) {
          break;
        }
        cout << "option " << long_options[option_index].name;
        if ( optarg ) cout << " with arg " << optarg;
        cout << endl;
        break;
      case 'h':
        this->helpFlag = 1;
        break;
      case 'u':
        this->usageFlag = 1;
        break;
      case 'v':
        this->versionFlag = 1;
        break;
      case 'a':
        this->actionName = optarg;
        break;
      case 'f':
        this->outputFormat = optarg;
        break;
      case 'o':
        this->outputFileFlag = 1;
        this->outputFilename = optarg;
        break;
      case '?': /* `getopt_long' prints error. */
        return 1;
      default:
        abort();
        break;
    }
  }
  if (outputFormat.empty()) {
    outputFormat.assign("plain");  // plain is default
  }
  PRINT_DEBUG("Processing of command-line done.");

  if (!this->helpFlag && !this->versionFlag && !this->usageFlag) {
    if (actionName.empty() && outputFormat != "help") {
      PRINT_ERROR("Desired action was not selected. Use '-a' flag.");
      this->usageFlag = 1;
      return 1;
    }

    string nextfilename;
    /* Going thru all of given input filenames */
    while (optind < this->argc) {
      nextfilename = string(*(this->argv + optind));
      this->inputFiles.push_back(nextfilename);
      ++optind;
    }
    PRINT_DEBUG("Given filenames:");
#if DEBUG == 1
    cerr << "\t";
    if (this->inputFiles.empty()) {
      cerr << "<none>";
    } else {
      for (unsigned int i = 0; i < this->inputFiles.size(); i++) {
        cerr << this->inputFiles[i] << "  ";
      }
    }
    cerr << endl;
    PRINT_DEBUG("Required action        : "+this->actionName);
    PRINT_DEBUG("Required output format : "+this->outputFormat);
#endif
  }
  return 0;
}

int OptionsHandler::isHelpFlag() {
  return this->helpFlag;
}

int OptionsHandler::isUsageFlag() {
  return this->usageFlag;
}

int OptionsHandler::isVersionFlag() {
  return this->versionFlag;
}

int OptionsHandler::isOutputFileFlag() {
  return this->outputFileFlag;
}

string OptionsHandler::getOutputFilename() {
  return this->outputFilename;
}

string OptionsHandler::getMessage() {
  return this->message;
}

string OptionsHandler::getActionName() {
  return this->actionName;
}

string OptionsHandler::getOutputFormat() {
  return this->outputFormat;
}

vector<string> OptionsHandler::getInputFilenames() {
  return this->inputFiles;
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
