/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Modulesmanager header file                                        }}}1*/

#ifndef SRC_MODULESMANAGER_H_
#define SRC_MODULESMANAGER_H_

#include "./typedefs.h"
#include "./container.h"

class ModulesManager {
  private:
    VEC_CNT containers;
    MAP_STR_STR knownModules;
    STR modules_dirname;

    /* IF 1  -> everything is ok, no export needed */
    /* IF 0  -> everything is ok, data are ready to export */
    /* IF -1 -> something in module failed, error! */
    int  retvalOfModule;

    int getFilenamesOfModules(STR dir, VEC_STR *files);
    STR getModuleName(STR filename);
    STR getInfoFromModule(STR filename);

  public:
    explicit ModulesManager(STR modules_dirname);
    virtual ~ModulesManager();
    virtual int initialize();
    virtual void listModules();
    virtual int runModule(STR moduleName);
    virtual int runModule(STR modoleName, SSTR *output);
    virtual void setContainers(VEC_CNT containers);
    virtual VEC_CNT getContainers();
    virtual int getRetvalOfModule();
};

#endif  // SRC_MODULESMANAGER_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
