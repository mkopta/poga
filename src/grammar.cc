/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Data structures holding grammar                                   }}}1*/

#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include "./data_types.h"

using std::string;
using std::vector;
using std::greater;
using data_types::Grammar;
using data_types::grammar_ns::Rule;
using data_types::grammar_ns::Element;

/* CLASS Element */
/* private:
  bool    is_terminal;
  bool    is_startSymb;
  string  name;
*****************************************************************************/

Element::Element(string _name, bool _isTerminal, bool _isStartSymb)
  :is_terminal(_isTerminal), is_startSymb(_isStartSymb), name(_name) {}

Element::Element(): is_terminal(false), is_startSymb(false), name("") {}

Element::~Element() {}

void Element::setIsTerminal(bool isTerminal) {
  this->is_terminal = isTerminal;
}

void Element::setIsStartSymb(bool isStartSymb) {
  this->is_startSymb = isStartSymb;
}

bool Element::isTerminal() const {
  return this->is_terminal;
}

bool Element::isStartSymb() const {
  return this->is_startSymb;
}

void Element::setName(string _name) {
  this->name = _name;
}

string Element::getName() const {
  return this->name;
}

bool Element::operator==(const Element &el) const {
  return this->name == el.name;
}

bool
Element::operator!=(const Element &el) const {
  return this->name != el.name;
}

bool Element::operator<(const Element &el) const {
  if (el.name == this->name) {
    return false;
  } else if (max(el.name, this->name, greater<string>()) == el.name) {
    return false;
  } else {
    return true;
  }
}

Element Element::operator=(const Element &el) {
  is_terminal = el.is_terminal;
  is_startSymb = el.is_startSymb;
  name = el.name;
  return el;
}

/* CLASS Rule */
/* private:
  Element         leftside;
  vector<Element> rightside;
  unsigned int    mark;
*****************************************************************************/

Rule::Rule(Element _leftside, vector<Element> _rightside)
  :mark(0), leftside(_leftside), rightside(_rightside) {}

Rule::Rule(Element _leftside, Element _rightside):mark(0), leftside(_leftside) {
  (this->rightside).push_back(_rightside);
}

Rule::Rule(Element _leftside):mark(0), leftside(_leftside) {}

Rule::Rule(): mark(0) {}

Rule::~Rule() {}

int Rule::getNext(Element *el) {
  if (this->mark >= (this->rightside).size()) {
    return -1;
  } else {
    *el = (this->rightside)[mark];
    return 0;
  }
}

int Rule::setMark(unsigned int mark) {
  if (this->mark >= (this->rightside).size()) {
    return -1;
  } else {
    this->mark = mark;
    return 0;
  }
}

void Rule::resetMark() {
  this->mark = 0;
}

int Rule::getElementAt(unsigned int position, Element *el) const {
  if (position >= (this->rightside).size()) {
    return -1;
  } else {
    *el =(this->rightside)[position];
    return 0;
  }
}

int Rule::changeElementAt(unsigned int position, Element newEl) {
  if (position >= (this->rightside).size()) {
    return -1;
  } else {
    (this->rightside)[position] = newEl;
    return 0;
  }
}

int
Rule::getFirst(Element * el) const {
  if ((this->rightside).empty()) {
    return -1;
  } else {
    *el =(this->rightside)[0];
    return 0;
  }
}

int Rule::getLeftside(Element *el) const {
  *el = this->leftside;
  return 0;
}

int
Rule::getRightside(vector<Element> * elements) const {
  if ((this->rightside).empty()) {
    return -1;
  } else {
    *elements = this->rightside;
    return 0;
  }
}

void Rule::setLeftside(Element el) {
  this->leftside = el;
}

void Rule::setRightside(vector<Element> elements) {
  this->rightside = elements;
}

void Rule::appendRightside(Element _el) {
  this->rightside.push_back(_el);
}

unsigned int Rule::getRightsideLenght() const {
  return (this->rightside).size();
}

bool Rule::rightsideIsEmpty() const {
  return (this->rightside).empty();
}

bool Rule::leftsideIsStartSymb() const {
  return (this->leftside).isStartSymb();
}

bool Rule::operator==(const Rule &el) const {
  if (el.leftside != this->leftside) {
    return false;
  }
  if (el.rightside.size() != this->rightside.size()) {
    return false;
  }
  if (this->rightside.size() == 0) {
    return true;
  }
  for (unsigned int i = 0; i < this->rightside.size(); i++)
    if (this->rightside[i] != el.rightside[i])
      return false;
  return true;
}

bool Rule::operator!=(const Rule &el) const {
  return !(this->operator == (el));
}

bool Rule::operator<(const Rule &el) const {
  // Operator < for object Rule is silly
  // but is needed by set<Rule>
  if (this->operator == (el)) {
    return false;
  }
  if (this->leftside != el.leftside) {
    if (this->leftside < el.leftside) {
      return true;
    } else {
      return false;
    }
  }
  if ((this->rightside).size() != el.rightside.size()) {
    if ((this->rightside).size() < el.rightside.size()) {
      return true;
    } else {
      return false;
    }
  }
  for (unsigned int i = 0; i < this->rightside.size(); i++) {
    if (this->rightside[i] != el.rightside[i]) {
      if (this->rightside[i] < el.rightside[i]) {
        return true;
      } else {
        return false;
      }
    }
  }
  return false;  // this will never happen
}

Rule
Rule::operator=(const Rule &el) {
  mark      = el.mark;
  leftside  = el.leftside;
  rightside = el.rightside;
  return el;
}

/* CLASS Grammar */
/* private:
  vector<Rule> rules;
  Element      init;
  string       comment;
*****************************************************************************/

Grammar::Grammar() {}

Grammar::Grammar(vector<Rule> rules) {
  this->rules = rules;
}

Grammar::Grammar(Rule rule) {
  (this->rules).push_back(rule);
}

Grammar::~Grammar() {}

void Grammar::addRule(Rule rule) {
  (this->rules).push_back(rule);
}

void Grammar::addRules(vector<Rule> rules) {
  for (unsigned int i = 0; i < rules.size(); i++)
    (this->rules).push_back(rules[i]);
}

int Grammar::getRule(unsigned int *rule_num, Rule *dest) const {
  if (*rule_num >=(this->rules).size()) {
    /* given rule_num(ber) is out of the range */
    return -1;
  } else {
    *dest =(this->rules)[*rule_num];
    return 0;
  }
}

int Grammar::getRules(vector<Rule> *rules) const {
  if ((this->rules).empty()) {
    return 1;
  } else {
    /* all ok */
    *rules = this->rules;
    return 0;
  }
}

int Grammar::changeRule(unsigned int *rule_num, Rule newrule) {
  if (*rule_num >= (this->rules).size()) {
    /* given rule_num(ber) is out of the range */
    return -1;
  } else {
    (this->rules)[*rule_num] = newrule;
    return 0;
  }
}

unsigned int Grammar::lines() const {
  return (this->rules).size();
}

vector<unsigned int> Grammar::getRulesNum(Element *nonterminal) const {
  /* foreach rule in rules:
   *   if leftside of rule equals given nonterminal:
   *     add rule number to output vector
   */
  vector<unsigned int> output;
  Element leftside;
  for (unsigned int i = 0; i < (this->rules).size(); i++) {
    (this->rules)[i].getLeftside(&leftside);

    if (leftside == *nonterminal) {
      output.push_back(i);
    }
  }
  return output;
}

int Grammar::getRightside(unsigned int *rule_num,
    vector<Element> *rightside) const {
  if (*rule_num >=(this->rules).size()) {
    return -1; /* rule doesn't exists */
  } else {
    int retval = 0;
    retval = (this->rules)[*rule_num].getRightside(rightside);
    if (retval != 0) return 1; /* rightside is empty */
    return 0; /* rightside is not empty, all ok */
  }
}

void Grammar::setInit(Element _init) {
  this->init = _init;
}

Element Grammar::getInit() const {
  return this->init;
}

void Grammar::setComment(string _comment) {
  this->comment = _comment;
}

string Grammar::getComment() const {
  return this->comment;
}

void Grammar::clear() {
  this->rules.clear();
  this->comment.erase();
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
