/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Manager of modules with algorithms                                }}}1*/

#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <dlfcn.h> /* dynamic objects support */
#include <cstring> /* strcpy */
#include <cctype> /* isalpha, isdigit, .. */
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm> /* sort */
#include "./modulesmanager.h"
#include "./container.h"
#include "./poga.h"

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::map;
using std::stringstream;

ModulesManager::ModulesManager(string modules_dirname)
  :retvalOfModule(0) {
  this->modules_dirname = modules_dirname;
}

ModulesManager::~ModulesManager() {}

int ModulesManager::initialize() {
  this->knownModules.clear();
  int num_of_modules = 0;
  string dir = this->modules_dirname;
  string mn;  // module name
  vector<string> files;

  if (this->getFilenamesOfModules(dir, &files) != 0) {
    PRINT_ERROR("Listing directory '" + dir + "' failed.");
    return -1;
  }
  if (files.empty()) {
    return 0;
  }

  unsigned int i;
  PRINT_DEBUG("Scanning for modules..");
  for (i = 0; i < files.size(); i++) {
    mn = getModuleName(string(files[i]));
    if (mn == "") {
      cerr << "Calling function 'getModuleName()' from module '" << files[i];
      cerr << "' failed. Please check this module manualy. ";
      return -1;
    } else {
      PRINT_DEBUG(files[i] << " - " << mn);
      // remember the filename and module name
      knownModules[mn] = string(files[i]);
      num_of_modules++;
    }
  }

  return num_of_modules;
}

void ModulesManager::setContainers(vector<Container> _containers) {
  this->containers = _containers;
}

vector<Container> ModulesManager::getContainers() {
  return this->containers;
}

void ModulesManager::listModules() {
  cout << "List of availible modules.." << endl;
  map <string, string>::iterator iter;
  for (iter = knownModules.begin(); iter != knownModules.end(); ++iter) {
    cout << endl;
    cout << iter->first << "(" << iter->second << ")" << endl;
    cout << getInfoFromModule(iter->second) << endl;
  }
  cout << endl;
}

string ModulesManager::getModuleName(string filename) {
  string temp = this->modules_dirname;
  temp += filename;

  void* handle = dlopen(temp.c_str(), RTLD_NOW);
  if (!handle) {
    cerr << "Cannot open module ";
    cerr << dlerror() << endl;
    return string("");
  }
  dlerror();

  /*
  This is how it should be, but it spits out an warning about void pointer
  typedef const char *(*getName_t)();
  getName_t getName =(getName_t) dlsym(handle, "getName");
  */

  /* This does work but it is bit ugly :(*/
  const char *(*getName)();
  void *ptr = dlsym(handle, "getName");
  memcpy(&getName, &ptr, sizeof(ptr));

  const char *dlsym_error = dlerror();
  if (dlsym_error) {
    cerr << "Cannot load symbol 'getName' from module ";
    cerr << ": " << dlsym_error << endl;
    dlclose(handle);
    return string("");
  }
  string module_name(getName());
  dlclose(handle);
  return module_name;
}

string ModulesManager::getInfoFromModule(string filename) {
  string temp = this->modules_dirname;
  temp += filename;

  void* handle = dlopen(temp.c_str(), RTLD_NOW);
  if (!handle) {
    cerr << "Cannot open module '" << temp << "': ";
    cerr << dlerror() << endl;
    return "Error happend during opening module";
  }
  dlerror();

  const char *(*getInfo)();
  void *ptr = dlsym(handle, "getInfo");
  memcpy(&getInfo, &ptr, sizeof(ptr));

  const char *dlsym_error = dlerror();
  if (dlsym_error) {
    cerr << "Cannot load symbol 'getInfo' from module '";
    cerr << temp << "': ";
    cerr << dlsym_error << endl;
    dlclose(handle);
    return "Error happend during opening module";
  }
  string info(getInfo());
  dlclose(handle);
  return info;
}

/*
 * Runs the user-defined module. Vector of input data containers must be ready
 * and ModulesManager must be initialized. The method 'runModule' will search
 * thru map of modules('knownModules') for 'moduleName' and translate it to
 * a filename. Then it joins path to modules with this filename, opens it,
 * loads the 'run' symbol and execute the 'run' method in module, giving the
 * vector<Container> to the algorithm. Then it close the module and ends.
 * Vector of containers is delivered into module method as pointer, so the
 * processed data are in modules manager immediately.
 *
 * @param moduleName a name of module to run
 * @return number 1 if desired module is not availible, 2 in case of
 *         error while working with a module, 0 if is all ok
 */
int ModulesManager::runModule(string moduleName) {
  map <string, string>::iterator iter;
  iter = knownModules.find(moduleName);
  if (iter == knownModules.end()) {
    return 1;  // Demanded module not found!
  }

  PRINT_DEBUG("Executing module '"<< iter->first <<
      "'(" <<iter->second << ").");
  string fullpath = this->modules_dirname;
  fullpath.append(iter->second);

  PRINT_DEBUG("Opening '" << fullpath << "' ..");

  void* handle = dlopen(fullpath.c_str(), RTLD_LAZY);
  if (!handle) {
    PRINT_ERROR("Cannot open library '" << fullpath << "', ");
    PRINT_ERROR(dlerror());
    return (2);  // error while opening
  }

  PRINT_DEBUG("Loading symbol run() ..");

  // reset errors
  dlerror();

  int (*run)(vector<Container> *);
  void *ptr = dlsym(handle, "run");
  memcpy(&run, &ptr, sizeof(ptr));

  const char *dlsym_error = dlerror();
  if (dlsym_error) {
    PRINT_ERROR("Cannot load symbol 'run', ");
    PRINT_ERROR(dlsym_error);
    dlclose(handle);
    return 2;  // error while opening
  }

  PRINT_DEBUG("Calling run() .. ");

  this->retvalOfModule = run(&(this->containers));

  PRINT_DEBUG("Closing '" << moduleName << "' module.");

  int dlclose_retval = dlclose(handle);
  if (dlclose_retval != 0) {
    PRINT_ERROR("Closing failed!");
    return 2;
  }
  return 0;
}

int ModulesManager::runModule(string moduleName, stringstream * output) {
  map <string, string>::iterator iter;
  iter = knownModules.find(moduleName);
  if (iter == knownModules.end()) {
    return 1;  // Module not found!
  }

  PRINT_DEBUG("Executing module '"<< iter->first <<
      "'(" <<iter->second << ").");
  string fullpath = this->modules_dirname;
  fullpath.append(iter->second);

  PRINT_DEBUG("Opening '" << fullpath << "' ..");

  void* handle = dlopen(fullpath.c_str(), RTLD_LAZY);
  if (!handle) {
    PRINT_ERROR("Cannot open library '" << fullpath << "', ");
    PRINT_ERROR(dlerror());
    return 2;  // error while opening
  }

  PRINT_DEBUG("Loading symbol run() ..");

  // reset errors
  dlerror();

  int (*run)(vector<Container> *, stringstream *);
  void *ptr = dlsym(handle, "run");
  memcpy(&run, &ptr, sizeof(ptr));

  const char *dlsym_error = dlerror();
  if (dlsym_error) {
    PRINT_ERROR("Cannot load symbol 'run', ");
    PRINT_ERROR(dlsym_error);
    dlclose(handle);
    return 2;  // error while opening
  }
  PRINT_DEBUG("Calling run() .. ");
  this->retvalOfModule = run(&(this->containers), output);
  PRINT_DEBUG("Closing '" << moduleName << "' module.");
  int dlclose_retval = dlclose(handle);
  if (dlclose_retval != 0) {
    PRINT_ERROR("Closing failed!");
    return 2;
  }
  return 0;
}

/*
 * Every module must be named as four digits number, then dot and then
 * the 'dll' or 'so' suffix (case insensitive).
 */
bool matchPattern(const char *a) {
  /* module filename format is: \d{4}.([sS][oO]|[dD][lL][lL]) */
  if (!isdigit(*a)) {
    return false;
  } else {
    a++;
  }
  if (!isdigit(*a)) {
    return false;
  } else {
    a++;
  }
  if (!isdigit(*a)) {
    return false;
  } else {
    a++;
  }
  if (!isdigit(*a)) {
    return false;
  } else {
    a++;
  }
  if (*a != '.') {
    return false;
  } else {
    a++;
  }
#ifdef POGA4WIN
  if (!(*a == 'd' || *a == 'D')) {
    return false;
  } else {
    a++;
  }
  if (!(*a == 'l' || *a == 'L')) {
    return false;
  } else {
    a++;
  }
  if (!(*a == 'l' || *a == 'L')) {
    return false;
  }
#elif defined(POGA4LIN)
  if (!(*a == 's' || *a == 'S')) {
    return false;
  } else {
    a++;
  }
  if (!(*a == 'o' || *a == 'O')) {
    return false;
  }
#endif
  return true;
}

int ModulesManager::getFilenamesOfModules(string dir, vector<string> *files) {
#ifdef POGA4LIN
  DIR *dp;
  struct dirent *dirp;
  if ((dp = opendir(dir.c_str())) == NULL) {
    cout << "Error(" << errno << ") opening '" << dir << "'" << endl;
    return errno;
  }

  string filename;
  while ((dirp = readdir(dp)) != NULL) {
    if (string(dirp->d_name) == string(".") ||
        string(dirp->d_name) == string("..")) {
      continue;
    }
    if (matchPattern(dirp->d_name)) {
      files->push_back(string(dirp->d_name));
    }
  }
  closedir(dp);
#elif defined(POGA4WIN)
#error "NOT IMPLEMENTED!"
#endif
  sort(files->begin(), files->end());
  return (0);
}

int ModulesManager::getRetvalOfModule() {
  return this->retvalOfModule;
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
