/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Parser is reading input files and fill the container              }}}1*/

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <set>
#include <cstdio> /* file handling */
#include <cctype> /* isalpha, isdigit, .. */
#include "./container.h"
#include "./parser.h"
#include "./poga.h"

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::stringstream;
using std::set;
/*
using namespace parser_ns;
using namespace data_types;
using namespace automaton_ns;
*/
using parser_ns::SubparserOfAutomaton;
using parser_ns::SubparserOfGrammar;
using parser_ns::SubparserOfPlainRegex;
using parser_ns::Lexan;

/* CLASS SubparserOfAutomaton */
/* private:
*****************************************************************************/

SubparserOfAutomaton::SubparserOfAutomaton(Lexan *_lex, const string _filename)
  :filename(_filename), lex(_lex), isset_states(false), isset_alpha(false),
  isset_init(false), isset_final(false), isset_trans(false) {}

SubparserOfAutomaton::~SubparserOfAutomaton() {}

int SubparserOfAutomaton::parse_states() {
  int token;
  bool is_initial = false;  // unknown in this part
  bool is_final = false;  // unknown in this part
  token =(*this->lex).getNextToken();
  if (token != Lexan::Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  while (true) {
    token =(*this->lex).getNextToken();

    if (token == Lexan::T_STRING) {
      string st_name =(*this->lex).getT_string();
      if (st_name.empty()) {
        PRINT_ERROR("Syntax error. Name of state can not be empty!" <<
            " Parsing section \"states\" failed.");
        PRINT_ERROR("Syntax error, file '" << this->filename << "'");
        return 1;
      }
      State  st(st_name, is_final, is_initial);
      this->parsed_states.insert(st);
    } else if (token == Lexan::T_RCB) {
#if DEBUG == 1
      set<State>::iterator it_dbg;
      stringstream ss;
      ss << "Found states: ";
      for (it_dbg = this->parsed_states.begin();
          it_dbg != this->parsed_states.end();
          it_dbg ++) {
        ss << "'" <<(*it_dbg).getName() << "' ";
      }
      PRINT_DEBUG(ss.str());
#endif
      return 0;
    } else {
      PRINT_ERROR("Syntax error, file '" << this->filename << "'");
      return 1;
    }
  }
  return 0;
}

int SubparserOfAutomaton::parse_alpha() {
  int    token;
  string symb;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  while (true) {
    token =(*this->lex).getNextToken();

    if (token == Lexan::T_STRING) {
      symb =(*this->lex).getT_string();
      if (symb.empty()) {
        PRINT_ERROR("Syntax error. Symbol can not be empty!" <<
            " Parsing section \"alpha\" failed.");
        PRINT_ERROR("Syntax error, file '" << this->filename << "'");
        return 1;
      }
      this->parsed_symbols.insert(symb);
    } else if (token == Lexan::T_RCB) {
#if DEBUG == 1
      set<string>::iterator it_dbg;
      stringstream ss;
      ss << "Found symbols: ";
      for (it_dbg = this->parsed_symbols.begin();
          it_dbg != this->parsed_symbols.end();
          it_dbg ++) {
        ss << "'" <<(*it_dbg) << "' ";
      }
      PRINT_DEBUG(ss.str());
#endif
      return 0;
    } else {
      PRINT_ERROR("Syntax error, file '" <<
          this->filename << "'");
      return 1;
    }
  }
  return 0;
}
int
SubparserOfAutomaton::parse_final(void) {
  int    token;
  bool   is_initial = false;  // unknown in this part
  bool   is_final   = true;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  while (true) {
    token =(*this->lex).getNextToken();

    if (token == Lexan::T_STRING) {
      string st_name =(*this->lex).getT_string();
      if (st_name.empty()) {
        PRINT_ERROR("Syntax error. Name of state can not be empty!" <<
            " Parsing section \"final\" failed.");
        PRINT_ERROR("Syntax error, file '" << this->filename << "'");
        return 1;
      }
      State  st(st_name, is_final, is_initial);
      this->parsed_finals.insert(st);
    } else if (token == Lexan::T_RCB) {
#if DEBUG == 1
      set<State>::iterator it_dbg;
      stringstream ss;
      ss << "Found final states: ";
      for (it_dbg = this->parsed_finals.begin();
          it_dbg != this->parsed_finals.end();
          it_dbg ++) {
        ss << "'" << it_dbg->getName() << "' ";
      }
      PRINT_DEBUG(ss.str());
#endif
      return 0;
    } else {
      PRINT_ERROR("Syntax error, file '" <<
          this->filename << "'");
      return 1;
    }
  }
  return 0;
}

int SubparserOfAutomaton::parse_init() {
  int    token;
  bool   is_initial = true;
  bool   is_final   = false;  // unknown in this part
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error in Init. " <<
        "Missing left curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  while (true) {
    token =(*this->lex).getNextToken();

    if (token == Lexan::T_STRING) {
      string st_name =(*this->lex).getT_string();
      if (st_name.empty()) {
        PRINT_ERROR("Syntax error. Name of state can not be empty!" <<
            " Parsing section \"init\" failed.");
        PRINT_ERROR("Syntax error, file '" << this->filename << "'");
        return 1;
      }
      State  st(st_name, is_final, is_initial);
      this->parsed_init.insert(st);
    } else if (token == Lexan::T_RCB) {
#if DEBUG == 1
      set<State>::iterator it_dbg;
      stringstream ss;
      ss << "Found init states: ";
      for (it_dbg = this->parsed_init.begin();
          it_dbg != this->parsed_init.end();
          it_dbg ++) {
        ss << "'" << it_dbg->getName() << "' ";
      }
      PRINT_DEBUG(ss.str());
#endif
      return 0;
    } else {
      PRINT_ERROR("Syntax error in Init, file '" <<
          this->filename << "'");
      return 1;
    }
  }
  return 0;
}

int
SubparserOfAutomaton::parse_trans(void) {
  int token;
  string src, symb, dest;
  bool is_initial = false;   // unknown yet
  bool is_final   = false;   // unknown yet;

  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  while (true) {
    token =(*this->lex).getNextToken();   // "src" OR end
    // syntax is: "src" "symb"=> "dest"
    if (token == Lexan::T_RCB) {
      // no further transitions defined
      break;
    } else if (token == Lexan::T_STRING) {
      src =(*this->lex).getT_string();
    } else {
      PRINT_ERROR("Syntax error. Missing left side of transition, " <<
          "file '" << this->filename << "'");
      return 1;
    }
    token =(*this->lex).getNextToken();  // "symb"
    if (token == Lexan::T_STRING) {
      symb =(*this->lex).getT_string();
    } else {
      PRINT_ERROR("Syntax error. Missing symbol, file '" <<
          this->filename << "'");
      return 1;
    }
    token =(*this->lex).getNextToken();  // arrow: =>
    if (token != Lexan::T_ARR) {
      PRINT_ERROR("Syntax error. Missing arrow, file '" <<
          this->filename << "'");
      return 1;
    }
    token =(*this->lex).getNextToken();  // "dest"
    if (token == Lexan::T_STRING) {
      dest =(*this->lex).getT_string();
    } else {
      PRINT_ERROR("Syntax error. Missing destination, file '" <<
          this->filename << "'");
      return 1;
    }

    /* now we can assembly new transition from collected data
     * IMPORTANT: the transitions can be added to an automaton AFTER
     * are added the states, symbols, init and final sections
     */
    State src_st(src , is_initial, is_final);
    State dest_st(dest, is_initial, is_final);
    PRINT_DEBUG("Found transition: \"" << src << "\" \"" << symb <<
        "\" => \"" << dest << "\"");
    Transition trans(src_st, symb, dest_st);
    this->parsed_transitions.insert(trans);
  }
#if DEBUG == 1
  PRINT_DEBUG("Checking set of parsed transitions..");
  set<Transition>::iterator it_trans;
  for (it_trans = this->parsed_transitions.begin();
      it_trans != this->parsed_transitions.end();
      it_trans++) {
    stringstream dbgss;
    dbgss << "Saved transition: \"";
    dbgss << it_trans->getSrc().getName();
    dbgss << "\" \"";
    dbgss << it_trans->getSymb();
    dbgss << "\" => \"";
    dbgss << it_trans->getDest().getName();
    dbgss << "\"";
    PRINT_DEBUG(dbgss.str());
  }
#endif /* DEBUG */

  return 0;
}
int
SubparserOfAutomaton::parse_comment(void) {
  int token;
  token = (*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  token = (*this->lex).getNextToken();
  if (token != Lexan::T_STRING) {
    PRINT_ERROR("Syntax error. Missing text in quotes, none" <<
        "given, file '" << this->filename << "'");
    return 1;
  }
  string t_string = (*this->lex).getT_string();
  this->parsed_comment.assign(t_string);
  token = (*this->lex).getNextToken();
  if (token != Lexan::T_RCB) {
    PRINT_ERROR("Syntax error. Missing right curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  return 0;
}

int SubparserOfAutomaton::parse() {
  int token;
  int retval;
  string ident;

  // parsing all sections
  while (true) {
    // get section name
    token = (*this->lex).getNextToken();
    if (token == Lexan::T_NULL) {
      break;
    } else if (token != Lexan::T_IDENT) {
      PRINT_ERROR("Syntax error. Missing identifier, file '" <<
          this->filename << "'");
      return 1;
    } else {
      ident = (*this->lex).getT_ident();
    }

    // parsing next section
    if (ident == "states") {
      PRINT_DEBUG("parsing section 'states'");
      retval = this->parse_states();
      this->isset_states = true;
    } else if (ident == "alphabet") {
      PRINT_DEBUG("parsing section 'alphabet'");
      retval = this->parse_alpha();
      this->isset_alpha = true;
    } else if (ident == "init") {
      PRINT_DEBUG("parsing section 'init'");
      retval = this->parse_init();
      this->isset_init = true;
    } else if (ident == "final") {
      PRINT_DEBUG("parsing section 'final'");
      retval = this->parse_final();
      this->isset_final = true;
    } else if (ident == "transitions") {
      PRINT_DEBUG("parsing section 'transitions'");
      retval = this->parse_trans();
      this->isset_trans = true;
    } else if (ident == "comment") {
      PRINT_DEBUG("parsing section 'comment'");
      retval = this->parse_comment();
    } else {
      PRINT_ERROR("Unknown section identifier: '" << ident <<
          "', file '" << this->filename << "'");
      return 1;
    }

    // check status of parsed section
    if (retval != 0) {
      PRINT_ERROR("Syntax error in section '" << ident << "'" <<
          ", file '" << this->filename << "'");
      return 1;
    }
  }

  bool allFound =
    this->isset_states &   // states must be defined
    this->isset_alpha  &   // input alphabet must be defined
    // FIXME
    /* for automata(at large) is INIT and FINAL not necessary
     * so it should be optional!
     */
    this->isset_init   &   // init must be defined  -- finite automata only
    this->isset_final  &   // final must be defined -- finite automata only
    this->isset_trans;     // transition must be set


  PRINT_DEBUG("parsing of file '" << this->filename << "' done."
      " Result: " <<(allFound?"OK":"ERR"));

  if (allFound) {
    return (0);
  } else {
    if (!this->isset_states) PRINT_ERROR("Missing states.");
    if (!this->isset_alpha)  PRINT_ERROR("Missing alphabet.");
    if (!this->isset_init)   PRINT_ERROR("Missing init.");
    if (!this->isset_final)  PRINT_ERROR("Missing final.");
    if (!this->isset_trans)  PRINT_ERROR("Missing transitions.");
    return (1);
  }
}

int
SubparserOfAutomaton::getAutomaton(Automaton *a) {
  /* We assume, that parsing went fine and we have all the
   * neccessary data. If something failed during parsing and
   * the parsed_XYZ are not filled, then it is error inside
   * the parsing process(it should return error)
   *
   * 0. prepare the AUTOMATON
   * 1. check if INIT states & FINAL states ARE contained
   *    IN STATES. Then GO THRU ALL STATES AND SET which
   *    is init & which is final
   * 2. check if all STATES IN TRANSITIONS are defined
   *    and append them info from states updated in step 1.
   *    Check if all SYMBOLS IN TRANSITIONS are defined
   * 3. INSERT all states  into automaton
   * 4. INSERT all symbols into automaton
   * 5. add all transitions
   * 6. append comment and set init state
   * 7. done
   */

  // 0.
  a->clear();

  // 1.
  set<State>::iterator it_1;
  set<State>::iterator it_1_1;

  if (this->parsed_init.empty()) {
    PRINT_ERROR("Semantic error. Automaton must have at least" <<
        " one init state but none defined, file '" <<
        this->filename << "'");
    return 1;
  }
  for (it_1 = this->parsed_init.begin();
      it_1 != this->parsed_init.end();
      it_1 ++) {
    it_1_1 = this->parsed_states.find(*it_1);
    if (it_1_1 == this->parsed_states.end()) {
      // defined init state is not declared in states
      PRINT_ERROR("Semantic error. " <<
          "Declared Init State '" <<
          (*it_1).getName() <<
          "' is not contained in States, file '" <<
          this->filename << "'");
      return 1;
    } else {
      /* state(it_1) is a init state so we mark it as final */
      State * sss = const_cast < State * >(&(*it_1_1));
      (*sss).setIsInit(true);
    }
  }

  for (it_1 = this->parsed_finals.begin();
      it_1 != this->parsed_finals.end();
      it_1 ++) {
    it_1_1 = this->parsed_states.find(*it_1);
    if (it_1_1 == this->parsed_states.end()) {
      // defined final state is not declared in states
      PRINT_ERROR("Semantic error. " <<
          "Declared Final State '" <<
          (*it_1).getName() <<
          "' is not contained in States, file '" <<
          this->filename << "'");
      return 1;
    } else {
      /* state(it_1) is a final state so we mark it as final */
      State * sss = const_cast < State * >(&(*it_1_1));
      (*sss).setIsFinal(true);
    }
  }

  // 2.
  set<Transition>::iterator it_2;
  set<State>::iterator it_2_1;
  // going thru all transitions
  for (it_2  = this->parsed_transitions.begin();
      it_2 != this->parsed_transitions.end();
      it_2 ++) {
    // check & update SRC
    it_2_1 = this->parsed_states.find((*it_2).src);
    if (it_2_1 == this->parsed_states.end()) {
      // SRC of actual transition is not contained in states
      PRINT_ERROR("Semantic error. " <<
          "Source state in transition begining with '" <<
          (*it_2).src.getName() << "' is not contained in States, "
          << "file '" << this->filename << "'");
      return 1;
    } else {
      // overwrite the state in trans. with updated state from step 1
      State * sss = const_cast < State * >(&((*it_2).src));
      *sss = *it_2_1;
    }
    // check & update DEST
    it_2_1 = this->parsed_states.find((*it_2).dest);
    if (it_2_1 == this->parsed_states.end()) {
      // DEST of actual transition is not contained in states
      PRINT_ERROR("Semantic error. " <<
          "Destination state in transition begining with '" <<
          (*it_2).src.getName() << "' is not contained in States, "
          << "file '" << this->filename << "'");
      return 1;
    } else {
      // overwrite the state in trans. with updated state from step 1
      State * sss = const_cast < State * >(&((*it_2).dest));
      *sss = *it_2_1;
    }
    // check SYMB
    if (this->parsed_symbols.count((*it_2).symb) == 0) {
      // symbol used in transition is not cointained in symbols
      PRINT_ERROR("Semantic error. In transition used symbol '"
          <<(*it_2).symb << "' is not cointained in symbols, "
          << "file '" << this->filename << "'");
      return 1;
    }
  }


  // 3.
  set<State>::iterator it_3;
  for (it_3 = this->parsed_states.begin();
      it_3 != this->parsed_states.end();
      it_3 ++) {
    a->addState(*it_3);
  }

  // 4.
  set<string>::iterator it_4;
  for (it_4 = this->parsed_symbols.begin();
      it_4 != this->parsed_symbols.end();
      it_4 ++) {
    a->addSymbol(*it_4);
  }

  // 5.
  // This must be done AFTER step 4. and 3. !
  set<Transition>::iterator it_5;
  for (it_5 = this->parsed_transitions.begin();
      it_5 != this->parsed_transitions.end();
      it_5 ++) {
    a->addTransition(*it_5);
  }

  // 6.
  set<State>::iterator it_6;
  set<State>::iterator it_6_1;
  a->setComment(this->parsed_comment);
  set<State> inits;
  for (it_6 = this->parsed_init.begin();
      it_6 != this->parsed_init.end();
      it_6 ++) {
    State st = *it_6;
    st.setIsInit(true);
    it_6_1 = this->parsed_finals.find(st);
    if (it_6_1 != this->parsed_finals.end())
      st.setIsFinal(true);

    inits.insert(st);
  }
  a->setInit(inits);

  // 7.
  return 0;
}

/* CLASS SubparserOfGrammar */
/* private:
*****************************************************************************/

SubparserOfGrammar::SubparserOfGrammar(Lexan *_lex, const string _filename)
  :filename(_filename) ,
  lex(_lex) ,
  isset_init(false) ,
  isset_terminals(false) ,
  isset_nonterminals(false) ,
  isset_rules(false)
{; }

SubparserOfGrammar::~SubparserOfGrammar() {}

int
SubparserOfGrammar::parse_init(void) {
  int token;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_STRING) {
    PRINT_ERROR("Syntax error. Missing name of init nonterminal,"
           " file '" << this->filename << "'");
    return (1);
  }
  string el_name =(*this->lex).getT_string();
  if (el_name.empty()) {
    PRINT_ERROR("Init is an empty string. This is not allowed.");
    return (1);
  }
  bool is_startSymb = true;
  bool is_terminal  = false;  // it has to be, but
  // we better check this out in SubparserOfGrammar::getGrammar
  Element el(el_name, is_terminal, is_startSymb);
  this->parsed_init = el;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_RCB) {
    PRINT_ERROR("Syntax error. Missing right curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  return (0);
}

int
SubparserOfGrammar::parse_rules() {
  int  token;
  bool is_term  = false;  // we don't know yet
  bool is_start = false;  // we don't know yet
  // left curly bracket
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }

  while (true) {  // parsing content
    token =(*this->lex).getNextToken();
    if (token == Lexan::T_RCB) {  // end of content
      return (0);
    } else if (token == Lexan::T_STRING) {
      // begin of rule
      string leftside_name =(*this->lex).getT_string();
      if (leftside_name.empty()) {
        PRINT_ERROR("Leftside of rule is an empty string. " <<
            "This is not allowed.");
        return (1);
      }
      Element leftside(leftside_name, is_term, is_start);
      // now should be arrow
      token =(*this->lex).getNextToken();
      if (token != Lexan::T_ARR) {
        PRINT_ERROR("Syntax error. Missing arrow('=>') "
            << "after leftside nonterminal, file '"
            << this->filename << "'");
        return (1);
      }
      // now is the rightside of rule, ending with dot(.)
      vector<Element> rightside;
      bool end = false;
      while (!end) {  // going thru rightside
        token =(*this->lex).getNextToken();
        if (token == Lexan::T_STRING) {  // next element of rightside
          string el_name =(*this->lex).getT_string();
          if (el_name.empty()) {
            PRINT_ERROR("Element of rule is an empty string. " <<
                "This is not allowed.");
            return (1);
          }
          Element el(el_name, is_term, is_start);
          rightside.push_back(el);
        } else if (token == Lexan::T_DOT) {  // end of rightside
          end = true;
        } else {  // unknown thing
          stringstream ss;
          ss << "Syntax error. ";
          ss << "Unknown symbol in rightside of the rule.";
          ss << " Expecting terminal/nonterminal or dot, ";
          ss << "file '" << this->filename << "'";
          PRINT_ERROR(ss.str());
          return (1);
        }
      }
      // now we have leftside & rightside
      // so we can create a Rule and insert it into
      // parsed rules
      Rule rl(leftside, rightside);
      this->parsed_rules.insert(rl);
    } else {
      stringstream ss;
      ss << "Syntax error. ";
      ss << "Expecting end of content(right curly ";
      ss << "bracket) or begin of rule(nonterminal), ";
      ss << "file '" << this->filename << "'";
      PRINT_ERROR(ss.str());
      return (1);
    }
  }
  PRINT_ERROR("This can't happen.");  // impossible
  return (0);
}

int
SubparserOfGrammar::parse_nonterminals(void) {
  int    token;
  bool   is_terminal  = false;
  bool   is_startSymb = false;  // unknown in this part
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  while (true) {
    token =(*this->lex).getNextToken();

    if (token == Lexan::T_STRING) {
      string el_name =(*this->lex).getT_string();
      if (el_name.empty()) {
        PRINT_ERROR("There is an empty string in nonterminals." <<
            " This is not allowed.");
        return (1);
      }
      Element el(el_name, is_terminal, is_startSymb);
      this->parsed_nonterminals.insert(el);
    } else if (token == Lexan::T_RCB) {
#if DEBUG == 1
      set<Element>::iterator it_dbg;
      stringstream ss;
      ss << "Found nonterminals: ";
      for (it_dbg = this->parsed_nonterminals.begin();
          it_dbg != this->parsed_nonterminals.end();
          it_dbg ++) {
        ss << "'" <<(*it_dbg).getName() << "' ";
      }
      PRINT_DEBUG(ss.str());
#endif
      return (0);
    } else {
      PRINT_ERROR("Syntax error, file '" <<
          this->filename << "'");
      return (1);
    }
  }
  // the cycle above has its own returns
  PRINT_ERROR("This can't happen.");
  return (0);
}

int
SubparserOfGrammar::parse_terminals(void) {
  int    token;
  bool   is_terminal  = true;
  bool   is_startSymb = false;  // unknown in this part
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  while (true) {
    token =(*this->lex).getNextToken();

    if (token == Lexan::T_STRING) {
      string el_name =(*this->lex).getT_string();
      if (el_name.empty()) {
        PRINT_ERROR("There is an empty string in terminals." <<
            " This is not allowed.");
        return 1;
      }
      Element el(el_name, is_terminal, is_startSymb);
      this->parsed_terminals.insert(el);
    } else if (token == Lexan::T_RCB) {
#if DEBUG == 1
      set<Element>::iterator it_dbg;
      stringstream ss;
      ss << "Found terminals: ";
      for (it_dbg = this->parsed_terminals.begin();
          it_dbg != this->parsed_terminals.end();
          it_dbg ++) {
        ss << "'" <<(*it_dbg).getName() << "' ";
      }
      PRINT_DEBUG(ss.str());
#endif
      return 0;
    } else {
      PRINT_ERROR("Syntax error, file '" <<
          this->filename << "'");
      return 1;
    }
  }
  // the cycle above has its own returns
  PRINT_ERROR("This can't happen.");
  return 0;
}

int
SubparserOfGrammar::parse_comment(void) {
  int token;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_STRING) {
    PRINT_ERROR("Syntax error. Missing text in quotes, none" <<
        "given, file '" << this->filename << "'");
    return 1;
  }
  string t_string =(*this->lex).getT_string();
  this->parsed_comment.assign(t_string);
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_RCB) {
    PRINT_ERROR("Syntax error. Missing right curly bracket, file '" <<
        this->filename << "'");
    return 1;
  }
  return 0;
}

int
SubparserOfGrammar::parse(void) {
  int    token;
  int    retval;
  string ident;

  // parsing all sections
  while (true) {
    // get section name
    token =(*this->lex).getNextToken();
    if (token == Lexan::T_NULL) {
      break;
    } else if (token != Lexan::T_IDENT) {
      PRINT_ERROR("Syntax error. Missing identifier, file '" <<
          this->filename << "'");
      return 1;
    } else {
      ident =(*this->lex).getT_ident();
    }

    // parsing next section
    if (ident == "init") {
      PRINT_DEBUG("parsing section 'init'");
      retval = this->parse_init();
      this->isset_init = true;
    } else if (ident == "terminals") {
      PRINT_DEBUG("parsing section 'terminals'");
      retval = this->parse_terminals();
      this->isset_terminals = true;
    } else if (ident == "nonterminals") {
      PRINT_DEBUG("parsing section 'nonterminals'");
      retval = this->parse_nonterminals();
      this->isset_nonterminals = true;
    } else if (ident == "rules") {
      PRINT_DEBUG("parsing section 'rules'");
      retval = this->parse_rules();
      this->isset_rules = true;
    } else if (ident == "comment") {
      PRINT_DEBUG("parsing section 'comment'");
      retval = this->parse_comment();
    } else {
      PRINT_ERROR("Unknown section identifier: '" << ident <<
          "', file '" << this->filename << "'");
      return 1;
    }

    // check status of parsed section
    if (retval != 0) {
      PRINT_ERROR("Syntax error in section '" << ident << "'" <<
          ", file '" << this->filename << "'");
      return 1;
    }
  }

  bool isOK =
    isset_rules &&
    isset_nonterminals &&
    isset_terminals &&
    isset_init;

  PRINT_DEBUG("parsing of file '" << this->filename << "' done."
      " Result: " <<(isOK?"OK":"ERR"));

  if (isOK) {
    return (0);
  } else {
    if (!isset_rules)
      PRINT_ERROR("Missing section \"rules\"");
    if (!isset_nonterminals)
      PRINT_ERROR("Missing section \"rules\"");
    if (!isset_terminals)
      PRINT_ERROR("Missing section \"terminals\"");
    if (!isset_init)
      PRINT_ERROR("Missing section \"init\"");

    return (1);
  }
}

int
SubparserOfGrammar::getGrammar(Grammar *g) {
  g->clear();

  // init nonterminal must be in nonterminals
  if (this->parsed_nonterminals.count(this->parsed_init) == 0) {
    PRINT_ERROR("Semantic error. Init nonterminal is not contained in "
        << "nonterminals, file '" << this->filename << "'");
    return (1);
  }
  // intersection of sets of NONTERMs and TERMs is empty
  set<Element>::iterator it_1;
  for (it_1 = this->parsed_terminals.begin();
      it_1 != this->parsed_terminals.end();
      it_1 ++) {
    if (this->parsed_nonterminals.count(*it_1) != 0) {
      PRINT_ERROR("Semantic error. Intersection of nonterminals and"
          " terminals must be empty set!, file '" << this->filename
          << "'");
      return (1);
    }
  }

  // going thru all parsed rules
  set<Rule>::iterator it_2;
  for (it_2 = this->parsed_rules.begin();
      it_2 != this->parsed_rules.end();
      it_2++) {
    Element         lside;
    vector<Element> rside;

(*it_2).getLeftside(&lside);
(*it_2).getRightside(&rside);

    if (this->parsed_nonterminals.count(lside) == 0) {
      PRINT_ERROR("Semantic error. Left side of rule is not "
          << "contained in nonterminals, file '" << this->filename
          << "'");
      return (1);
    }

    if (lside == this->parsed_init)
      lside.setIsStartSymb(true);

    for (unsigned int y = 0; y < rside.size(); y ++) {  // going thru rside
      if (this->parsed_nonterminals.count(rside[y]) != 0) {
(rside[y]).setIsTerminal(false);
      } else if (this->parsed_terminals.count(rside[y]) != 0) {
(rside[y]).setIsTerminal(true);
      } else {
        PRINT_ERROR("Semantic error. In rules described symbol '" <<
(rside[y]).getName() << "' is not contained in nonterms"
            " nor terminals, file '" << this->filename << "'");
        return (1);
      }

      if (rside[y] == this->parsed_init) {
        (rside[y]).setIsStartSymb(true);
      }
    }

    // all ok, we add this rule into grammar
    Rule r(lside, rside);
    g->addRule(r);
  }

  Element init_el = this->parsed_init;
  init_el.setIsTerminal(false);
  init_el.setIsStartSymb(true);
  g->setInit(init_el);

  return (0);
}

/* CLASS SubparserOfPlainRegex */
/* private:
*****************************************************************************/

SubparserOfPlainRegex::SubparserOfPlainRegex(Lexan *_lex,
    const string _filename):
  filename(_filename), lex(_lex), isset_plainregex(false) {}

SubparserOfPlainRegex::~SubparserOfPlainRegex() {}

int
SubparserOfPlainRegex::parse_plainregex(void) {
  int token;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_STRING) {
    PRINT_ERROR("Syntax error. Missing plain regex content, none" <<
        "given, file '" << this->filename << "'");
    return (1);
  }
  this->parsed_plainRegex =(*this->lex).getT_string();
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_RCB) {
    PRINT_ERROR("Syntax error. Missing right curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }

  PRINT_DEBUG("Parsed plainregex: '" << this->parsed_plainRegex << "'");
  return (0);
}

int
SubparserOfPlainRegex::parse_comment(void) {
  int token;
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_LCB) {
    PRINT_ERROR("Syntax error. Missing left curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_STRING) {
    PRINT_ERROR("Syntax error. Missing text in quotes, none" <<
        "given, file '" << this->filename << "'");
    return (1);
  }
  string t_string =(*this->lex).getT_string();
  this->parsed_comment.assign(t_string);
  token =(*this->lex).getNextToken();
  if (token != Lexan::T_RCB) {
    PRINT_ERROR("Syntax error. Missing right curly bracket, file '" <<
        this->filename << "'");
    return (1);
  }
  return (0);
}

int
SubparserOfPlainRegex::parse(void) {
  int    token;
  int    retval;
  string ident;

  // parsing all sections
  while (true) {
    // get section name
    token =(*this->lex).getNextToken();
    if (token == Lexan::T_NULL) {
      break;
    } else if (token != Lexan::T_IDENT) {
      PRINT_ERROR("Syntax error. Missing identifier, file '" <<
          this->filename << "'");
      return (1);
    } else {
      ident =(*this->lex).getT_ident();
    }

    // parsing next section
    if (ident == "plainregex") {
      PRINT_DEBUG("parsing section 'plainregex'");
      retval = this->parse_plainregex();
      this->isset_plainregex = true;
    } else if (ident == "comment") {
      PRINT_DEBUG("parsing section 'comment'");
      retval = this->parse_comment();
    } else {
      PRINT_ERROR("Unknown section identifier: '" << ident <<
          "', file '" << this->filename << "'");
      return 1;
    }

    // check status of parsed section
    if (retval != 0) {
      PRINT_ERROR("Syntax error in section '" << ident << "'" <<
          ", file '" << this->filename << "'");
      return 1;
    }
  }

  PRINT_DEBUG("parsing of file '" << this->filename << "' done."
      " Result: " <<(this->isset_plainregex?"OK":"ERR"));


  if (!isset_plainregex) {
    PRINT_ERROR("Missing section \"plainregex\"!");
    return 1;
  } else {
    return 0;
  }
}

int SubparserOfPlainRegex::getPlainRegex(PlainRegex *pr) {
  if (!this->parsed_plainRegex.empty()) {
    pr->setExpression(this->parsed_plainRegex);
    pr->setComment(this->parsed_comment);
    return 0;
  } else {
    PRINT_ERROR("Plain regex cannot be empty!");
    return 1;
  }
}

/* CLASS Lexan */
/* private:
     string   filename;
     FILE   * fp;
     bool     opened;
     string   t_string;
     string   t_ident;
     char     unknown_char;
*****************************************************************************/


Lexan::Lexan(string _filename)
  :filename(_filename) ,
  opened(true) ,
  unknown_char('\0') {
  fp = fopen(filename.c_str() , "rt");
  if (!fp) { this->opened = false; }
}

Lexan::~Lexan() {
  if (this->opened) { fclose(this->fp); }
}

int Lexan::getNextToken() {
  int c;  // readed character from file
  int st = 0;  // lexan state

  if (!this->fp) {
    return Lexan::T_NOTFILE;
  }

  this->unknown_char = ' ';
  this->t_string.clear();
  this->t_ident.clear();

  while (1) {
    c = fgetc(this->fp);
    switch (st) {
      case 0 :  // state 0
        if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
          /* whitespace is ignored */
          break;
        } else if (c == '{') {
          return Lexan::T_LCB;
        } else if (c == '}') {
          return Lexan::T_RCB;
        } else if (c == '.') {
          return Lexan::T_DOT;
        } else if (c == '-' || c == '=') {  // begin of arrow?
          st = 1;  // go to state 1
          break;
        } else if (c == EOF) {
          return Lexan::T_NULL;
        } else if (c == '"') {  // begin of quoted string
          st = 2;
          break;
        } else if (isalpha(c)) {  // begin of identifier
          this->t_ident.push_back(static_cast<char>(c));
          st = 4;
          break;
        } else {  // unknown element
          this->unknown_char = static_cast<char>(c);
          return Lexan::T_UNKNOWN;
        }
      case 1 :  // state 1, arrow
        if (c == '>') {
          return (Lexan::T_ARR);
        } else {
          this->unknown_char = static_cast<char>(c);
          return (Lexan::T_UNKNOWN);
        }
      case 2 :  // state 2, quoted
        if (c == '"') {  // end of quoting
          return (Lexan::T_STRING);
        } else if (c == 92) {  // escape sequence,(92 is backslash)
          st = 3;
          break;
        } else {  // just another character in quoting
          this->t_string.push_back(static_cast<char>(c));
          break;
        }
      case 3 :  // state 3, quoted, escaped
        /* 92 is backslash, 34 is doublequote
         * in quoted string can be only \\ or \"
         */
        if (c == 92 || c == 34) {
          this->t_string.push_back(static_cast<char>(c));
          st = 2;
          break;
        } else {
          this->unknown_char = static_cast<char>(c);
          return (Lexan::T_UNKNOWN);
        }
      case 4 :  // identifier
        // allowed characters in identifier: [a-zA-Z][a-zA-Z0-9_-]*
        if (isalpha(c) || isdigit(c) || c == '_' || c == '-') {
          this->t_ident.push_back(static_cast<char>(c));
          break;
        } else if (c == ' ' || c == '\r' || c == '\n' || c == '\t') {
          // end of identifier
          ungetc(c, this->fp);
          return (Lexan::T_IDENT);
        } else {  // unknown character
          this->unknown_char = static_cast<char>(c);
          return (Lexan::T_UNKNOWN);
        }
    }
  }
}

string Lexan::getT_ident() {
  return this->t_ident;
}

string Lexan::getT_string() {
  return this->t_string;
}

char Lexan::getUnknownChar() {
  return this->unknown_char;
}

bool Lexan::isReady() {
  return this->opened;
}


/* CLASS Parser */
/* private:
*****************************************************************************/

Parser::Parser() {}
Parser::~Parser() {}

int Parser::files2containers(vector<string> inputFilenames,
    vector<Container> &containers) {
  string actualFileName;
  string fileContentType;

  containers.clear();
  // loop over files
  for (unsigned int i = 0; i < inputFilenames.size(); i++) {
    actualFileName = inputFilenames[i];
    PRINT_DEBUG("Transforming '" + actualFileName + "' to container.");
    /* Transformation */
    Container cont;
    Lexan     lex(actualFileName);
    int       token;

    if (!lex.isReady()) {
      PRINT_ERROR("Cannot open file '" << actualFileName << "' !");
      return 1;
    }

    token = lex.getNextToken();
    if (token != Lexan::T_IDENT || lex.getT_ident() != "type") {
      PRINT_ERROR("Syntax error. Expected identifier 'type', "
          "file '" << actualFileName << "'");
      return 1;
    }

    token = lex.getNextToken();
    if (token != Lexan::T_LCB) {
      PRINT_ERROR("Syntax error. Expected 'left curly bracket', "
          "file '" << actualFileName << "'");
      return 1;
    }

    token = lex.getNextToken();
    if (token != Lexan::T_STRING) {
      PRINT_ERROR("Syntax error. Expected 'quoted string', "
          "file '" << actualFileName << "'");
      return 1;
    } else {
      fileContentType = lex.getT_string();
    }

    token = lex.getNextToken();
    if (token != Lexan::T_RCB) {
      PRINT_ERROR("Syntax error. Expected 'right curly bracket', "
          "file '" << actualFileName << "'");
      return 1;
    }

    // we have got : type { "something" }
    PRINT_DEBUG("File '" << actualFileName << "' describes '" <<
        fileContentType << "'");

    // depending on the "something" we decide what to do
    if (fileContentType == "automaton") {
      SubparserOfAutomaton soa(&lex, actualFileName);
      int retval = soa.parse();
      if (retval != 0) {
        PRINT_ERROR("Error while parsing automaton, file '" <<
            actualFileName << "'");
        return 1;
      } else {
        Automaton a;
        int soa_retval;
        soa_retval = soa.getAutomaton(&a);
        if (soa_retval != 0) {
          PRINT_ERROR("Semantic error in file '" << actualFileName
              << "'");
          return 1;
        }
        cont.setAutomaton(a);
      }
    } else if (fileContentType == "grammar") {
      SubparserOfGrammar sog(&lex, actualFileName);
      int retval = sog.parse();
      if (retval != 0) {
        PRINT_ERROR("Error while parsing grammar, file '" <<
            actualFileName << "'");
        return 1;
      } else {
        Grammar g;
        int sog_retval;
        sog_retval = sog.getGrammar(&g);
        if (sog_retval != 0) {
          PRINT_ERROR("Semantic error in file '" << actualFileName
              << "'");
          return 1;
        }
        cont.setGrammar(g);
      }
    } else if (fileContentType == "plainregex") {
      SubparserOfPlainRegex sopr(&lex, actualFileName);
      int retval = sopr.parse();
      if (retval != 0) {
        PRINT_ERROR("Error while parsing plain regex, file '" <<
            actualFileName << "'");
        return 1;
      } else {
        PlainRegex pr;
        int sopr_retval;
        sopr_retval = sopr.getPlainRegex(&pr);
        if (sopr_retval != 0) {
          PRINT_ERROR("Semantic error in file '" << actualFileName
              << "'");
          return 1;
        }
        cont.setPlainRegex(pr);
      }
    } else {
      PRINT_ERROR("Unknown content type '" << fileContentType << "'"
          << ", file '" << actualFileName << "'");
      return 1;
    }

    /* end of transformation */
    containers.push_back(cont);
  }
  return 0;
}

/* EOF */
