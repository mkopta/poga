/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Container header file for poga                                    }}}1*/

#ifndef SRC_CONTAINER_H_
#define SRC_CONTAINER_H_

#include "./data_types.h"

class Container {
  private:
    data_types::Automaton automaton;
    data_types::Grammar grammar;
    data_types::PlainRegex plainregex;

    bool cont_automaton;
    bool cont_grammar;
    bool cont_plainregex;

  public:
    Container();
    virtual ~Container();

    virtual void setAutomaton(data_types::Automaton automaton);
    virtual void setGrammar(data_types::Grammar grammar);
    virtual void setPlainRegex(data_types::PlainRegex plainregex);

    virtual data_types::Automaton getAutomaton() const;
    virtual data_types::Grammar getGrammar() const;
    virtual data_types::PlainRegex getPlainRegex() const;

    virtual bool contains_automaton() const;
    virtual bool contains_grammar() const;
    virtual bool contains_plainregex() const;

    virtual void clear();
};

#endif  // SRC_CONTAINER_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
