/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Print_automaton_info module for poga system                       }}}1*/

#include <iostream>
#include <set>
#include <sstream>
#include <vector>
#include <string>
#include "./container.h"
#include "./data_types.h"
#include "./poga.h"

using std::set;
using std::string;
using std::stringstream;
using std::vector;
using data_types::Automaton;
using data_types::automaton_ns::State;

bool is_nfa(const Automaton &a) {
  if (a.getInit().size() > 1)
    return true;
  set<State> states = a.getStates();
  set<string> symbols = a.getSymbols();
  set<string>::iterator it_str;
  while (not states.empty()) {
    State st = *(states.begin());
    states.erase(states.begin());
    for (it_str = symbols.begin(); it_str != symbols.end(); it_str ++)
      if (a.cross(st, *it_str).size() > 1)
        return true;
  }

  return false;
}

int process(Container const &c) {
  if (!c.contains_automaton()) {
    PRINT_ERROR("Given container does not contain an automaton!");
    return 1;
  }

  Automaton a = c.getAutomaton();

  stringstream ss;

  ss << "Automaton has " << endl;
  ss << "\t" << a.getStates().size() << " states" << endl;
  ss << "\t" << a.getTransitions().size() << " transitions" << endl;
  ss << "\t" << a.getUnusedSymb().size() << " unused symbols" << endl;
  ss << "\t" << a.getDiscreteStates().size() << " discrete states" << endl;
  if (!a.getComment().empty()) {
    ss << "Comment for this automaton -----------" << endl;
    ss << a.getComment() << endl;
    ss << "- end of comment ---------------------" << endl;
  }

  ss << "Automaton is ";
  if (is_nfa(a)) ss << "non";
  ss << "deterministic" << endl;

  cout << ss.str();

  return 0;
}

extern "C" int run(vector<Container> *containers) {
  int containers_processed = 0;
  int retval = 0;
  for (unsigned int i = 0; i <(*containers).size(); i ++) {
    PRINT_DEBUG("Processing the " <<(i+1) << "th container");
    retval = process((*containers)[i]);
    if (retval != 0) {
      PRINT_ERROR("Processing the " <<(i+1) <<
          "th container failed.");
      PRINT_DEBUG("Total processed containers: " <<
          containers_processed);
      return -1;
    } else {
      containers_processed++;
    }
  }
  PRINT_DEBUG("Total processed containers: " << containers_processed);
  return 1;
}

extern "C" const char* getName(void) {
  return "print_automaton_info";
}

extern "C" const char* getInfo(void) {
  return "Prints detail information about given automaton.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
