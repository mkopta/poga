/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
preg2aut module for poga system                                   }}}1*/

#include <sstream>
#include <cassert>
#include <set>
#include <vector>
#include "./poga.h"
#include "./container.h"

using std::vector;
using std::stringstream;
using std::set;
using data_types::PlainRegex;
using data_types::Automaton;
using data_types::automaton_ns::State;

int preg2aut(PlainRegex & preg, Automaton & aut) {
  /* System doesn't allow empty PlainRegex's */
  assert(preg.length() > 0);
  preg.setMark(0U);
  stringstream comment;
  comment << "This automaton is based on plainregex '";
  int i = 0;
  char c;
  set<State> inits;
  while ((c = preg.getNextChar()) != 0) {
    stringstream src_name;
    src_name << i;
    stringstream dest_name;
    dest_name <<(i+1);
    stringstream symbol;
    symbol << c;
    comment << c;
    State src(src_name.str(), false, (i == 0));
    State dest(dest_name.str(), ((i+1) == preg.length()), false);
    aut.addTransition(src, symbol.str(), dest);
    if (i == 0) inits.insert(src);
    i++;
  }
  comment << "'";
  aut.setComment(comment.str());
  aut.setInit(inits);
  return 0;
}

int process_container(Container *c) {
  if (!c->contains_plainregex()) {
    PRINT_ERROR("Given container does not contain an PlainRegex!");
    return 1;
  }
  PlainRegex preg = c->getPlainRegex();
  Automaton  aut;
  if (preg2aut(preg, aut) != 0) {
    return 1;
  }
  c->clear();
  c->setAutomaton(aut);
  return 0;
}

extern "C" int run(vector<Container> *containers) {
  if (containers->size() != 1) {
    PRINT_ERROR("Module preg2aut needs EXACTLY ONE input container, but "
        << containers->size() << " given.");
    return -1;
  }
  if (process_container(&((*containers)[0])) != 0) {
    return-1;
  } else {
    return 0;
  }
}

extern "C" const char* getName(void) {
  return "preg2aut";
}

extern "C" const char* getInfo(void) {
  return "Converts a plain regular expression to an automaton.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
