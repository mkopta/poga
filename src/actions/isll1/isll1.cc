/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
isll1 module for poga system                                      }}}1*/

#include <vector>
#include <sstream>
#include <set>
#include "./poga.h"
#include "./container.h"

using std::vector;
using std::set;
using std::stringstream;
using data_types::Grammar;
using data_types::grammar_ns::Rule;
using data_types::grammar_ns::Element;

bool isll1 = true;

void create_keys(
    const vector<Rule> & rules,
    const Element      & epsilon,
    vector<Element>    & nkeys,
    vector<Element>    & tkeys) {
  set<Element> nonterminals;
  set<Element> terminals;
  for (unsigned int i = 0; i < rules.size(); i++) {
    Element leftside;
    vector<Element> rightside;
    rules[i].getLeftside(&leftside);
    rules[i].getRightside(&rightside);
    if (leftside.isTerminal()) {
      terminals.insert(leftside);
    } else {
      nonterminals.insert(leftside);
    }
    for (unsigned int j = 0; j < rightside.size(); j++) {
      if (rightside[j].isTerminal()) {
        terminals.insert(rightside[j]);
      } else {
        nonterminals.insert(rightside[j]);
      }
    }
  }
  nkeys.clear();
  tkeys.clear();
  set<Element>::iterator it;
  for (it = nonterminals.begin(); it != nonterminals.end(); it++)
    nkeys.push_back(*it);
  for (it = terminals.begin(); it != terminals.end(); it++)
    tkeys.push_back(*it);

  tkeys.push_back(epsilon);
}

void insert_all(const set<Element> & from, set<Element> & to) {
  set<Element>::iterator it;
  for (it = from.begin(); it != from.end(); it++)
    to.insert(*it);
}

void calculate_firsts(
    const vector<Rule>     & rules,
    const vector<Element>  & nkeys,
    const Element          & epsilon,
    vector< set<Element> > & FIRSTs) {
  PRINT_DEBUG("calculating FIRSTs");

  FIRSTs.clear();
  for (unsigned int i = 0; i < nkeys.size(); i++) {
    // vector N and vector FIRSTs are connected by same indexes
    set<Element> empty;
    FIRSTs.push_back(empty);
  }

  bool changing;

  do {
    changing = false;

    for (unsigned int i = 0; i < rules.size(); i++) {
      Element leftside;
      rules[i].getLeftside(&leftside);

      if (rules[i].getRightsideLenght() == 0) {
        for (unsigned int j = 0; j < nkeys.size(); j++)
          if (nkeys[j] == leftside) {
            int s1 = FIRSTs[j].size();
            FIRSTs[j].insert(epsilon);
            int s2 = FIRSTs[j].size();
            if (s1 != s2) changing = true;
            break;
          }
      } else {
        Element right_0;
        rules[i].getElementAt(0U, &right_0);
        if (right_0.isTerminal()) {
          for (unsigned int j = 0; j < nkeys.size(); j++)
            if (nkeys[j] == leftside) {
              int s1 = FIRSTs[j].size();
              FIRSTs[j].insert(right_0);
              int s2 = FIRSTs[j].size();
              if (s1 != s2) changing = true;
              break;
            }
        } else {
          unsigned int k;
          for (k = 0; k < nkeys.size(); k++)
            if (nkeys[k] == right_0) break;

          set<Element>::iterator it;
          if (FIRSTs[k].count(epsilon) == 0) {
            for (unsigned int j = 0; j < nkeys.size(); j++)
              if (nkeys[j] == leftside) {
                int s1 = FIRSTs[j].size();
                insert_all(FIRSTs[k], FIRSTs[j]);
                int s2 = FIRSTs[j].size();
                if (s1 != s2) changing = true;
                break;
              }
          } else {
            //  FIRST(leftside) =
            // (first(right_0) - epsilon) union FIRST(right_1)

            set<Element> tmp;
            tmp = FIRSTs[k];
            tmp.erase(epsilon);
            if (rules[i].getRightsideLenght() > 1) {
              Element right_1;
              rules[i].getElementAt(1U, &right_1);
              for (unsigned int k2 = 0; k2 < nkeys.size(); k2++)
                if (nkeys[k2] == right_1) {
                  insert_all(FIRSTs[k2], tmp);
                  break;
                }
            }
            for (unsigned int j = 0; j < nkeys.size(); j++) {
              if (nkeys[j] == leftside) {
                int s1 = FIRSTs[j].size();
                insert_all(tmp, FIRSTs[j]);
                int s2 = FIRSTs[j].size();
                if (s1 != s2) changing = true;
                break;
              }
            }
          }
        }
      }
    }
  } while (changing);
  PRINT_DEBUG("calculating FIRST done");
}

void calculate_follows(const vector<Rule> &rules, const vector<Element> &nkeys,
    const Element &epsilon, const vector< set<Element> > &FIRSTs,
    vector< set<Element> > &FOLLOWs) {
  PRINT_DEBUG("calcucalting FOLLOW");

  FOLLOWs.clear();
  for (unsigned int i = 0; i < nkeys.size(); i++) {
    set<Element> init;
    if (nkeys[i].isStartSymb()) init.insert(epsilon);
    FOLLOWs.push_back(init);
  }


  bool changed;
  do {
    changed = false;
    for (unsigned int i = 0; i < rules.size(); i++) {
      Element leftside;
      vector<Element> rightside;
      rules[i].getLeftside(&leftside);
      rules[i].getRightside(&rightside);

      for (unsigned int j = 0; j < rightside.size(); j++) {
        if (rightside[j].isTerminal()) continue;

        Element alpha = rightside[j];
        unsigned int id_alpha;
        for (id_alpha = 0; id_alpha < nkeys.size(); id_alpha++)
          if (alpha == nkeys[id_alpha]) break;

        bool add_follow_leftside = true;

        for (unsigned int k = j+1; k < rightside.size(); k++) {
          Element beta = rightside[k];

          if (beta.isTerminal()) {
            int s1 = FOLLOWs[id_alpha].size();
            FOLLOWs[id_alpha].insert(beta);
            int s2 = FOLLOWs[id_alpha].size();
            if (s1 != s2) changed = true;

            add_follow_leftside = false;
            break;
          }

          // beta is nonterminal
          unsigned int id_beta;
          for (id_beta = 0; id_beta < nkeys.size(); id_beta++)
            if (nkeys[id_beta] == beta) break;


          // FOLLOW(Left) := FOLLOW(Alpha) u(FIRST(Beta) - Eps)
          set<Element> tmp = FIRSTs[id_beta];
          tmp.erase(epsilon);
          int s1 = FOLLOWs[id_alpha].size();
          insert_all(tmp, FOLLOWs[id_alpha]);
          int s2 = FOLLOWs[id_alpha].size();
          if (s1 != s2) changed = true;

          // beta nonterminal has an epsilon in FIRST
          if (FIRSTs[id_beta].count(epsilon) == 0) {
            add_follow_leftside = false;
            break;
          }
        }

        if (add_follow_leftside) {
          unsigned int id_left;
          for (id_left = 0; id_left < nkeys.size(); id_left++)
            if (nkeys[id_left] == leftside) break;

          int s1 = FOLLOWs[id_alpha].size();
          insert_all(FOLLOWs[id_left], FOLLOWs[id_alpha]);
          int s2 = FOLLOWs[id_alpha].size();
          if (s1 != s2) changed = true;
        }
      }
    }
  } while (changed);
  PRINT_DEBUG("calculating FOLLOW done");
}

void clear_cube(const vector<Element> &nkeys, const vector<Element> &tkeys,
    vector< vector< set<unsigned int> > > &cube) {
  set<unsigned int> empty;
  vector< set<unsigned int> > tmp;

  cube.clear();

  for (unsigned int t = 0; t < tkeys.size(); t++)
    tmp.push_back(empty);

  for (unsigned int n = 0; n < nkeys.size(); n++)
    cube.push_back(tmp);
}

void raise_cube(const vector<Rule> &rules, const vector<Element> &nkeys,
    const vector<Element> &tkeys, const Element &epsilon,
    const vector< set<Element> > &FIRSTs, const vector< set<Element> > &FOLLOWs,
    vector< vector< set<unsigned int> > > &cube) {
  /*  Pseudocode:
  foreach rule
    Left = getLeft
    Rightside = getRight

    if Rightside empty
      foreach element from FOLLOW(Left)
        cube[Left,element].add(rule.num)
      continue

    right_0 = Rightside[0]

    if right_0 isTerminal
      cube[Left,right_0].add(rule.num)
      continue

    foreach element from FIRST(right_0)
      if element = epsilon
        there_was_epsilon = yes
        continue
      else cube[Left,element].add(rule.num)

    if there_was_epsilon
      foreach element from FOLLOW(right_0)
        cube[Left,element].add(rule.num)
  */
  clear_cube(nkeys, tkeys, cube);

  for (unsigned int rule_num = 0; rule_num < rules.size(); rule_num++) {
    Element left;
    vector<Element> rightside;
    rules[rule_num].getLeftside(&left);
    rules[rule_num].getRightside(&rightside);

    unsigned int id_left;
    for (id_left = 0; id_left < nkeys.size(); id_left++)
      if (nkeys[id_left] == left) break;

    if (rightside.empty()) {
      set<Element> follow = FOLLOWs[id_left];
      set<Element>::iterator el;
      for (el = follow.begin(); el != follow.end(); el++) {
        unsigned int id_el;
        for (id_el = 0; id_el < tkeys.size(); id_el++)
          if (tkeys[id_el] == *el) break;

        cube[id_left][id_el].insert(rule_num);
      }
      continue;
    }

    Element right_0 = rightside[0];

    if (right_0.isTerminal()) {
      unsigned int id_right_0;
      for (id_right_0 = 0; id_right_0 < tkeys.size(); id_right_0++)
        if (tkeys[id_right_0] == right_0) break;

      cube[id_left][id_right_0].insert(rule_num);
      continue;
    }

    unsigned int id_right_0;
    for (id_right_0 = 0; id_right_0 < nkeys.size(); id_right_0++)
      if (nkeys[id_right_0] == right_0) break;

    set<Element> first = FIRSTs[id_right_0];
    set<Element>::iterator el;
    bool there_was_epsilon = false;

    for (el = first.begin(); el != first.end(); el++) {
      if (*el == epsilon) {
        there_was_epsilon = true;
        continue;
      }

      unsigned int id_el;
      for (id_el = 0; id_el < tkeys.size(); id_el++)
        if (tkeys[id_el] == *el) break;

      cube[id_left][id_el].insert(rule_num);
    }

    if (there_was_epsilon) {
      set<Element> follow = FOLLOWs[id_right_0];
      for (el = follow.begin(); el != follow.end(); el++) {
        unsigned int id_el;
        for (id_el = 0; id_el < tkeys.size(); id_el++)
          if (tkeys[id_el] == *el) break;

        cube[id_left][id_el].insert(rule_num);
      }
    }
  }
}

void show_results(
    const vector<Rule>           & rules,
    const vector<Element>        & nkeys,
    const vector<Element>        & tkeys,
    const vector< set<Element> > & FIRSTs,
    const vector< set<Element> > & FOLLOWs,
    const vector< vector< set<unsigned int> > > & cube
) {
  stringstream out;

  out << "=== RULES ===" << endl;
  for (unsigned int rule_num = 0; rule_num < rules.size(); rule_num++) {
    Element leftside;
    vector<Element> rightside;
    rules[rule_num].getLeftside(&leftside);
    rules[rule_num].getRightside(&rightside);

    out << rule_num+1U << ") ";

    out << "\"" << leftside.getName() << "\" => ";
    for (unsigned int rs = 0; rs < rightside.size(); rs++) {
      out << "\"" << rightside[rs].getName() << "\" ";
    }
    out << endl;
  }

  set<Element>::iterator it;
  set<unsigned int>::iterator uiit;

  out << "=== FIRST ===" << endl;
  for (unsigned int i = 0; i < FIRSTs.size(); i++) {
    out << "\"" << nkeys[i].getName() << "\" { ";
    for (it = FIRSTs[i].begin(); it != FIRSTs[i].end(); it++)
      out << "\"" <<(*it).getName() << "\" ";
    out << "}" << endl;
  }

  out << "=== FOLLOW ===" << endl;
  for (unsigned int i = 0; i < FOLLOWs.size(); i++) {
    out << "\"" << nkeys[i].getName() << "\" { ";
    for (it = FOLLOWs[i].begin(); it != FOLLOWs[i].end(); it++)
      out << "\"" <<(*it).getName() << "\" ";
    out << "}" << endl;
  }

  out << "=== Parsing function ===" << endl;
  for (unsigned int i = 0; i < cube.size(); i++) {
    for (unsigned int j = 0; j < cube[i].size(); j++) {
      if (cube[i][j].empty()) continue;

      out << "M(\"" << nkeys[i].getName() << "\", \"";
      out << tkeys[j].getName() << "\") = ";
      set<unsigned int> rules_id = cube[i][j];

      for (uiit = rules_id.begin(); uiit != rules_id.end(); uiit++) {
        out <<((*uiit)+1) << " ";
      }

      if (rules_id.size() > 1) {
        out << "\t!!! CONFLICT !!!";
        isll1 = false;
      }

      out << endl;
    }
  }

  out << "=== Conclusion ===" << endl;
  if (isll1) {
    out << "Given grammar IS LL(1).";
  } else {
    out << "Given grammar IS NOT LL(1).";
  }
  cout << out.str() << endl;
}

void analyze(vector<Rule> & rules) {
  const Element epsilon = Element("", false, false);
  vector<Element> nkeys;  // list of unique nonterminals, their pos id their ID
  vector<Element> tkeys;  // list of unique terminals, their pos id their ID
  vector< set<Element> > FIRSTs;
  vector< set<Element> > FOLLOWs;
  /* parsing table
   * First index is ID of nonterminal(from nkeys)
   * Second index is ID of terminal(from tkeys)
   * the third is id(s) of rule(s) to use(from rules) */
  vector< vector< set<unsigned int> > > cube;

  create_keys(rules, epsilon, nkeys, tkeys);
  calculate_firsts(rules, nkeys, epsilon, FIRSTs);
  calculate_follows(rules, nkeys, epsilon, FIRSTs, FOLLOWs);
  raise_cube(rules, nkeys, tkeys, epsilon, FIRSTs, FOLLOWs, cube);

  show_results(rules, nkeys, tkeys, FIRSTs, FOLLOWs, cube);
}

extern "C" int run(vector<Container> *containers) {
  if (containers->size() != 1) {
    PRINT_ERROR("Module isll1 needs EXACTLY ONE input container, but "
        << containers->size() << " given.");
    return -1;
  }

  if (!(*containers)[0].contains_grammar()) {
    PRINT_ERROR("Given container does not contain a grammar!");
    return -1;
  }

  Grammar g =(*containers)[0].getGrammar();
  vector<Rule> rules;

  if (g.getRules(&rules) == 1) {
    cout << "Given grammar has no rules! So, It's LL(0)." << endl;
    return 1;
  } else {
    analyze(rules);
  }

  return 1;
}

extern "C" const char* getName(void) {
  return "isll1";
}

extern "C" const char* getInfo(void) {
  return "Check if given grammar is LL(1) by calculating FIRST and \
FOLLOW sets,\n\
constructing parsing table and checking for conflicts. \
It also gives \nnice overview about given grammar.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
