/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
nfa2dfa module for poga system                                    }}}1*/

#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <cassert>
#include "./poga.h"
#include "./container.h"

using std::map;
using std::pair;
using std::set;
using std::string;
using std::stringstream;
using std::vector;
using std::cerr;
using std::endl;
using data_types::Automaton;
using data_types::automaton_ns::State;
using data_types::automaton_ns::HalfTransition;

typedef unsigned int UI;
typedef set<State> POWERSET;

vector<POWERSET> states;
vector< map<string, POWERSET> > trans;
set<POWERSET> todo;

template <class T >
void insert_all(const set<T> &from, set<T> &to) {
  typename set<T>::iterator it;
  for (it = from.begin(); it != from.end(); it++)
    to.insert(*it);
}

set<HalfTransition> find_transitions(const Automaton &aut, const POWERSET &ps) {
  set<HalfTransition> ht;
  set<HalfTransition> tmpht;
  POWERSET::iterator ps_it;

  // foreach state in given powerstate find all transitions and unite them all
  for (ps_it = ps.begin(); ps_it != ps.end(); ps_it++) {
    tmpht = aut.expand(*ps_it);
#if DEBUG == 1
    set<HalfTransition>::iterator itdbg;
    for (itdbg = tmpht.begin(); itdbg != tmpht.end(); itdbg++) {
      cerr << "EXPAND: \"" << itdbg->getSymb() << "\" => \"";
      cerr << itdbg->getDest().getName() << "\"";
      cerr << (itdbg->getDest().isInitial() ? "T" : "F");
      cerr << (itdbg->getDest().isFinal() ? "T" : "F");
      cerr << endl;
    }
#endif
    insert_all(tmpht, ht);
    tmpht.clear();
  }
  return ht;
}

map<string, POWERSET> aggregate_by_symbols(const set<HalfTransition> &ht) {
  map<string, POWERSET> mm;
  map<string, POWERSET>::iterator it_mm;
  set<HalfTransition>::iterator it_ht;
  for (it_ht = ht.begin(); it_ht != ht.end(); it_ht++) {
    it_mm = mm.find(it_ht->getSymb());
    if (it_mm == mm.end()) {
      // create new powerset
      POWERSET p;
      p.insert(it_ht->getDest());
      mm.insert(pair<string, POWERSET> (it_ht->getSymb(), p));
    } else {
      // add state into powerset
      it_mm->second.insert(it_ht->getDest());
    }
  }
  return mm;
}

bool in_states(const POWERSET &ps) {
  for (UI i = 0; i < states.size(); i++)
    if (ps == states[i])
      return true;
  return false;
}

void register_all_new_states(map<string, POWERSET> *tr) {
  map<string, POWERSET>::iterator it;
  for (it = tr->begin(); it != tr->end(); it++)
    if (!in_states(it->second)) {
#if DEBUG == 1
      cerr << "NEW STATES: ";
      POWERSET::iterator itdbg;
      cerr << "{ ";
      for (itdbg = (it->second).begin(); itdbg != (it->second).end(); itdbg++)
        cerr << "\"" << itdbg->getName() << "\" ";
      cerr << " }" << endl;
#endif
      todo.insert(it->second);
    }
}

State translate_powerset2state(const POWERSET &ps, const vector<State> &st) {
  for (UI i = 0; i < states.size(); i++)
    if (states[i] == ps )
      return st[i];
  PRINT_ERROR("There is a really big bug in this module.");
  exit(1);
}

void add_transitions(Automaton &a, const vector<State> &dfa_states) {
  map<string, POWERSET>::iterator it_mm;
  for (UI i = 0; i < dfa_states.size(); i++) {
    for (it_mm = trans[i].begin(); it_mm != trans[i].end(); it_mm++) {
      State src = dfa_states[i];
      string symb = it_mm->first;
      State dest = translate_powerset2state(it_mm->second, dfa_states);
      a.addTransition(src, symb, dest);
    }
  }
}

vector<State> create_states_from_powersets(const vector<POWERSET> &powersets) {
  vector<State> states;
  POWERSET::iterator p_it;
  bool is_initial;
  bool is_final;
  for (UI i = 0; i < powersets.size(); i++) {
    is_initial = (i == 0);
    is_final = false;
#if DEBUG == 1
    cerr << "Creating new state from powerset: { ";
#endif
    for (p_it = powersets[i].begin(); p_it != powersets[i].end(); p_it++) {
      if (p_it->isFinal()) is_final = true;
#if DEBUG == 1
      cerr << "\"" << p_it->getName() << "\"";
      cerr << (p_it->isInitial() ? "T" : "F");
      cerr << (p_it->isFinal() ? "T" : "F");
      cerr << " ";
#endif
    }
#if DEBUG == 1
    cerr << "}" << endl;
#endif
    stringstream ss;
    ss << "Q" << i;
    State st(ss.str(), is_final, is_initial);
    PRINT_DEBUG("New state: \"" << ss.str() << "\"" <<
        (is_initial ? "T" : "F") << (is_final ? "T" : "F"));
    states.push_back(st);
  }
  return states;
}

Automaton create_automaton() {
  Automaton a;
  vector<State> dfa_states;
  dfa_states = create_states_from_powersets(states);
  assert(dfa_states.size() != (states.size() != trans.size()));
  add_transitions(a, dfa_states);
  set<State> inits;
  inits.insert(dfa_states[0]);
  a.setInit(inits);
  a.setComment("Converted from another automaton using nfa2dfa poga module");
  return a;
}

#if DEBUG == 1
void print_todo() {
  set<POWERSET>::iterator it1;
  cerr << "== TODO begin" << endl;
  for (it1 = todo.begin(); it1 != todo.end(); it1++) {
    cerr << "{ ";
    POWERSET::iterator it2;
    for (it2 = it1->begin(); it2 != it1->end(); it2++) {
      cerr << "\"" << it2->getName() << "\"";
      cerr << (it2->isInitial()?"T":"F");
      cerr << (it2->isFinal()?"T":"F");
      cerr << " ";
    }
    cerr << "}" << endl;
  }
  cerr << "== TODO end" << endl;
}
#endif

Automaton nfa2dfa(const Automaton &nfa) {
  POWERSET inits = nfa.getInit();
  todo.insert(inits);
  while (!todo.empty()) {
    PRINT_DEBUG("Processing next new state..");
#if DEBUG == 1
    print_todo();
#endif
    POWERSET ps;
    ps = *(todo.begin());
    todo.erase(ps);

    set<HalfTransition> ht = find_transitions(nfa, ps);
    map<string, POWERSET> tr = aggregate_by_symbols(ht);
    states.push_back(ps);
    trans.push_back(tr);
    register_all_new_states(&tr);
  }
  PRINT_DEBUG("Creating new automaton");
  Automaton dfa = create_automaton();
  return dfa;
}

extern "C" int run(vector<Container> *containers) {
  if (containers->size() != 1) {
    PRINT_ERROR("Module nfa2dfa needs EXACTLY ONE input container, but "
        << containers->size() << " given.");
    return -1;
  }
  if (!(*containers)[0].contains_automaton()) {
    PRINT_ERROR("Given container does not contain a automaton!");
    return -1;
  }
  Automaton nfa = (*containers)[0].getAutomaton();
  PRINT_DEBUG("Converting to DFA");
  Automaton dfa = nfa2dfa(nfa);
  PRINT_DEBUG("Work done");
  containers->clear();
  Container c;
  c.setAutomaton(dfa);
  containers->push_back(c);
  return 0;
}

extern "C" const char* getName(void) {
  return "nfa2dfa";
}

extern "C" const char* getInfo(void) {
  return "Convert given nondeterministic automaton to deterministic.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
