/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Complement module for poga system                                 }}}1*/

#include <set>
#include <sstream>
#include <string>
#include <vector>
#include "./poga.h"
#include "./container.h"

using std::set;
using std::vector;
using std::string;
using std::stringstream;
using data_types::Automaton;
using data_types::automaton_ns::State;
using data_types::automaton_ns::Transition;
using data_types::automaton_ns::HalfTransition;

template <class T >
void insert_all(const set<T> & from, set<T> & to) {
  typename set<T>::iterator it;
  for (it = from.begin(); it != from.end(); it ++)
    to.insert(*it);
}

State create_garbage_state(const set<State> & existing_states) {
  const string basename("GARBAGE_STATE");
  string gs_name(basename);
  bool is_final = false;  // will be inverted later
  bool is_init  = false, found = false;
  int  suffix   = 0;
  set<State>::iterator it_st;
  while (true) {
    for (it_st = existing_states.begin();
        it_st != existing_states.end(); it_st ++)
      if (it_st->getName() == gs_name) found = true;
    if (!found) {
      break;
    } else {
      stringstream ss;
      ss << basename << "_" << suffix++;
      gs_name = ss.str();
    }
  }
  State gs(gs_name, is_final, is_init);
  return gs;
}

set<Transition> create_all_gs2gs_trans(
    const State       & gs,
    const set<string> & symbols) {
  set<Transition> trans;
  set<string>::iterator it_str = symbols.begin();
  for (; it_str != symbols.end(); it_str ++) {
    Transition tr(gs, *it_str, gs);
    trans.insert(tr);
  }
  return trans;
}

bool add_all_missing_transitions(Automaton & a, const State & garbage_state) {
  bool changed = false;
  set<State> todo = a.getStates();
  set<string> symbols = a.getSymbols();
  while (not todo.empty()) {
    State st = *(todo.begin());
    todo.erase(todo.begin());
    set<HalfTransition> ht = a.expand(st);
    set<string>::iterator it_sy = symbols.begin();
    for (; it_sy != symbols.end(); it_sy ++) {
      bool found = false;
      set<HalfTransition>::iterator it_ht = ht.begin();
      for (; it_ht != ht.end(); it_ht ++) {
        if (it_ht->getSymb() == *it_sy) {
          found = true;
          break;
        }
      }
      if (!found) {
        Transition tr(st, *it_sy, garbage_state);
        a.addTransition(tr);
        changed = true;
      }
    }
  }
  return changed;
}

void invert_all_states(Automaton & aut) {
  Automaton newaut;
  set<State>::iterator      it_st;
  set<string>::iterator     it_str;
  set<Transition>::iterator it_tr;

  set<State> init = aut.getInit();
  set<State> newinit;
  for (it_st = init.begin(); it_st != init.end(); it_st ++) {
    State st(it_st->getName(), !it_st->isFinal(), it_st->isInitial());
    newinit.insert(st);
  }
  newaut.setInit(newinit);

  set<string> symb = aut.getUnusedSymb();
  for (it_str = symb.begin(); it_str != symb.end(); it_str ++)
    newaut.addSymbol(*it_str);

  set<State> states = aut.getDiscreteStates();
  for (it_st = states.begin(); it_st != states.end(); it_st ++) {
    State st(it_st->getName(), !it_st->isFinal(), it_st->isInitial());
    aut.addState(st);
  }

  set<Transition> trans = aut.getTransitions();
  for (it_tr = trans.begin(); it_tr != trans.end(); it_tr ++) {
    State src = it_tr->getSrc();
    src.setIsFinal(!src.isFinal());
    State dest = it_tr->getDest();
    dest.setIsFinal(!dest.isFinal());
    Transition tr(src, it_tr->getSymb(), dest);
    newaut.addTransition(tr);
  }

  newaut.setComment(aut.getComment());

  aut = newaut;
}

Automaton create_complement(const Automaton orig) {
  PRINT_DEBUG("Creating complement..");
  Automaton c(orig);  // complement
  State gs = create_garbage_state(c.getStates());
  set<Transition> t_gs = create_all_gs2gs_trans(gs, c.getSymbols());
  bool changed = add_all_missing_transitions(c, gs);
  if (changed) {
    set<Transition>::iterator it_tr = t_gs.begin();
    for (; it_tr != t_gs.end(); it_tr ++)
      c.addTransition(*it_tr);
  }
  invert_all_states(c);  // do final(s):=!final(s) on all states(incl. gs)
  return c;
}

extern "C" int run(vector<Container> * containers) {
  if (containers->size() != 1) {
    PRINT_ERROR("Module runFA needs EXACTLY ONE input container, but "
        << containers->size() << " given.");
    return -1;
  }
  if (!(*containers)[0].contains_automaton()) {
    PRINT_ERROR("Given container does not contain a automaton!");
    return -1;
  }
  Automaton complement = create_complement((*containers)[0].getAutomaton());
  containers->clear();
  Container cont;
  cont.setAutomaton(complement);
  containers->push_back(cont);
  return 0;
}

extern "C" const char * getName(void) {
  return "complement";
}

extern "C" const char * getInfo(void) {
  return "Create a complement to a given automaton.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
