/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
runFA module for poga system                                      }}}1*/

#include <iostream>
#include <sstream>
#include <string>
#include <set>
#include <vector>
#include "./poga.h"
#include "./container.h"

using std::set;
using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::vector;
using data_types::Automaton;
using data_types::automaton_ns::State;

template <class T >
void insert_all(const set<T> & from, set<T> & to) {
  typename set<T>::iterator it;
  for (it = from.begin(); it != from.end(); it ++)
    to.insert(*it);
}

bool in_alpha(const set<string> & alpha, const string & s) {
  set<string>::iterator it;
  it = alpha.find(s);
  if (it == alpha.end()) {
    return false;
  } else {
    return true;
  }
}

void print_alphabet(const set<string> & alpha) {
  set<string>::iterator it;
  cout << "Automaton alphabet: { ";
  for (it = alpha.begin(); it != alpha.end(); it ++)
    cout << "\"" << *it << "\" ";
  cout << "}" << endl;
}

void print_actual_state(const set<State> & st) {
  if (st.empty()) {
    cout << "Actual state: GARBAGE STATE" << endl;
    return;
  }
  set<State>::iterator it_st;
  cout << "Actual state: { ";
  for (it_st = st.begin(); it_st != st.end(); it_st ++)
    cout << "\"" << it_st->getName() << "\" ";
  cout << "}" << endl;
}

void print_intro(void) {
  cout << "Automaton ready. Enter next symbol or ";
  cout << "just hit enter to stop(also ^D)." << endl << endl;
}

bool actual_state_is_final(const set<State> & state) {
  set<State>::iterator it_st;
  for (it_st = state.begin(); it_st != state.end(); it_st++) {
    if (it_st->isFinal()) {
      return true;
    } else {
      return false;
    }
  }
  return false;  // this should never happen
}

void step(const Automaton & aut, set<State> & state, const string & symbol) {
  set<State> new_state;
  set<State>::iterator  it_st;
  while (not /*old*/state.empty()) {
    State st = *(state.begin());
    state.erase(state.begin());
    set<State> dest = aut.cross(st, symbol);
    insert_all(dest, new_state);
  }
  state = new_state;
}

bool runFA(const Automaton &aut) {
  set<string> alpha = aut.getAlphabet();
  set<State> actual_state = aut.getInit();
  print_intro();
  string input;
  print_actual_state(actual_state);
  print_alphabet(alpha);
  while (true) {
    cout << "Input symbol: ";
    getline(cin, input);
    if (input.empty()) break;
    if (not in_alpha(alpha, input)) {
      cout << "Given symbol is not in automaton alphabet." << endl;
      print_alphabet(alpha);
    } else {
      step(aut, actual_state, input);
    }
    print_actual_state(actual_state);
  }
  return actual_state_is_final(actual_state);
}

extern "C" int run(vector<Container> *containers) {
  if (containers->size() != 1) {
    PRINT_ERROR("Module runFA needs EXACTLY ONE input container, but "
        << containers->size() << " given.");
    return(-1);
  }
  if (!(*containers)[0].contains_automaton()) {
    PRINT_ERROR("Given container does not contain a automaton!");
    return -1;
  }
  Automaton aut = (*containers)[0].getAutomaton();
  bool accept = runFA(aut);
  cout << endl;
  if (accept) {
    cout << "ACCEPTED" << endl;
  } else {
    cout << "NOT ACCEPTED" << endl;
  }
  return 1;
}

extern "C" const char* getName(void) {
  return "runFA";
}

extern "C" const char* getInfo(void) {
  return "Run given automaton step by step while asking user for symbols.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
