/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Parser header file for poga                                       }}}1*/

#ifndef SRC_PARSER_H_
#define SRC_PARSER_H_

#include <cstdio>
#include "./typedefs.h"

// forward declaration
class Container;

using data_types::Automaton;
using data_types::Grammar;
using data_types::PlainRegex;
using data_types::automaton_ns::State;
using data_types::automaton_ns::Transition;
using data_types::grammar_ns::Rule;
using data_types::grammar_ns::Element;

namespace parser_ns {
  class Lexan {
    private:
      STR filename;
      FILE *fp;
      bool opened;
      /* captured data */
      STR t_string;
      STR t_ident;
      /* in case of syntax error */
      char unknown_char;
    public:
      enum TOKEN {
        T_NOTFILE,   // file read error
        T_NULL,      // EOF & first state
        T_UNKNOWN,   // unknown element, error
        T_IDENT,     // identifier
        T_STRING,    // "anything"
        T_LCB,       // left  curly bracket
        T_RCB,       // right curly bracket
        T_ARR,       // arrow => or ->
        T_DOT        // .
      };
      explicit Lexan(STR filename);  // open file
      virtual ~Lexan();  // close file
      virtual int getNextToken();
      virtual STR getT_ident();
      virtual STR getT_string();
      virtual char getUnknownChar();
      virtual bool isReady();
  };

  class SubparserOfAutomaton {
    private:
      const STR filename;
      Lexan *lex;
      bool isset_states;
      bool isset_alpha;
      bool isset_init;
      bool isset_final;
      bool isset_trans;

      SET_ST parsed_states;
      SET_STR parsed_symbols;
      SET_TR parsed_transitions;
      SET_ST parsed_finals;
      SET_ST parsed_init;
      STR parsed_comment;

      virtual int parse_states();
      virtual int parse_alpha();
      virtual int parse_init();
      virtual int parse_final();
      virtual int parse_trans();
      virtual int parse_comment();
    public:
      SubparserOfAutomaton(Lexan *_lex, const STR _filename);
      virtual ~SubparserOfAutomaton();
      virtual int parse();
      virtual int getAutomaton(Automaton *a);
  };

  class SubparserOfGrammar {
    private:
      const STR filename;
      Lexan *lex;

      bool isset_init;
      bool isset_terminals;
      bool isset_nonterminals;
      bool isset_rules;

      EL parsed_init;
      SET_EL parsed_terminals;
      SET_EL parsed_nonterminals;
      SET_RU parsed_rules;
      STR parsed_comment;

      virtual int parse_init();
      virtual int parse_rules();
      virtual int parse_nonterminals();
      virtual int parse_terminals();
      virtual int parse_comment();
    public:
      SubparserOfGrammar(Lexan *_lex, const STR _filename);
      virtual ~SubparserOfGrammar();
      virtual int parse();
      virtual int getGrammar(Grammar *g);
  };

  class SubparserOfPlainRegex {
    private:
      const STR filename;
      Lexan *lex;
      STR parsed_plainRegex;
      STR parsed_comment;
      bool isset_plainregex;

      virtual int parse_plainregex();
      virtual int parse_comment();
    public:
      SubparserOfPlainRegex(Lexan *_lex, const STR _filename);
      virtual ~SubparserOfPlainRegex();
      virtual int parse();
      virtual int getPlainRegex(PlainRegex *pr);
  };
}

class Parser {
  public:
    Parser();
    virtual ~Parser();
    virtual int files2containers(VEC_STR inputFilenames,
        VEC_CNT &containers);
};


#endif  // SRC_PARSER_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
