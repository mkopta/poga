/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Controller header file for poga                                   }}}1*/

#ifndef SRC_CONTROLLER_H_
#define SRC_CONTROLLER_H_

#include "./typedefs.h"

class OptionsHandler;

class Controller {
  private:
    STR version;
    STR progname;
    int argc;
    char** argv;

    virtual int  processCmdline(OptionsHandler *optionshandler);
    virtual void printVersion();
    virtual void printUsage();
    virtual void printHelp();
    virtual STR findPrefix(STR arg0);
  public:
    Controller(int argc, char **argv, const STR version);
    virtual ~Controller();
    virtual int run();
};

#endif  // SRC_CONTROLLER_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
