/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Container holds input data in inner data structures.              }}}1*/

// One Container object respresents one input file

#include "./poga.h"
#include "./typedefs.h"
#include "./data_types.h"
#include "./container.h"

using data_types::Automaton;
using data_types::Grammar;
using data_types::PlainRegex;

Container::Container(): automaton(), grammar(), plainregex(),
  cont_automaton(false), cont_grammar(false), cont_plainregex(false) {}

Container::~Container() {}

void Container::setAutomaton(data_types::Automaton _automaton) {
  this->automaton = _automaton;
  this->cont_automaton = true;
}

void Container::setGrammar(data_types::Grammar _grammar) {
  this->grammar = _grammar;
  this->cont_grammar = true;
}

void Container::setPlainRegex(data_types::PlainRegex _plainregex) {
  this->plainregex = _plainregex;
  this->cont_plainregex = true;
}

Automaton Container::getAutomaton() const {
  return this->automaton;
}

Grammar Container::getGrammar() const {
  return this->grammar;
}

PlainRegex Container::getPlainRegex() const {
  return this->plainregex;
}

bool Container::contains_automaton() const {
  return this->cont_automaton;
}

bool Container::contains_grammar() const {
  return this->cont_grammar;
}

bool Container::contains_plainregex() const {
  return this->cont_plainregex;
}

void Container::clear() {
  this->cont_automaton = false;
  this->cont_grammar = false;
  this->cont_plainregex = false;

  Automaton a;
  Grammar g;
  PlainRegex pr;
  this->automaton = a;
  this->grammar = g;
  this->plainregex = pr;
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
