/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
'dot' export module for poga system                               }}}1*/

#include <sstream>
#include <vector>
#include <string>
#include <set>
#include "./poga.h"
#include "./container.h"

using std::stringstream;
using std::set;
using std::string;
using std::vector;
using data_types::Automaton;
using data_types::automaton_ns::Transition;
using data_types::automaton_ns::State;

void
export_automaton(const Automaton & a, stringstream * output) {
  stringstream              trans;
  set<string>               init;
  set<string>               finals;
  set<string>               discrete;
  set<string>::iterator     it_str;
  set<Transition>           tr      = a.getTransitions();
  set<Transition>::iterator it_tr;

  // go thru all transitions
  for (it_tr = tr.begin(); it_tr != tr.end(); it_tr ++) {
    if (it_tr->getSrc().isFinal())
      finals.insert(it_tr->getSrc().getName());
    if (it_tr->getDest().isFinal())
      finals.insert(it_tr->getDest().getName());

    if (it_tr->getSrc().isInitial())
      init.insert(it_tr->getSrc().getName());
    if (it_tr->getDest().isInitial())
      init.insert(it_tr->getDest().getName());

    trans << "\t\"" << it_tr->getSrc().getName() << "\"";
    trans << " -> ";
    trans << "\"" << it_tr->getDest().getName() << "\"";
    trans << " [ label = \"" << it_tr->getSymb() << "\" ];";
    trans << endl;
  }

  // the rest for corectness
  set<State> inits;
  set<State>::iterator it_st;
  for (it_st = inits.begin(); it_st != inits.end(); it_st ++)
    init.insert(it_st->getName());
  set<State> discrete_st;
  discrete_st = a.getDiscreteStates();
  for (it_st = discrete_st.begin(); it_st != discrete_st.end(); it_st ++) {
    discrete.insert(it_st->getName());
    if (it_st->isFinal())
      finals.insert(it_st->getName());
  }

  // mix the soup..
  *output << "digraph automaton" << endl << "{" << endl;

  *output << "\tnode [ shape = doublecircle ];" << endl;
  *output << "\t";
  for (it_str = finals.begin(); it_str != finals.end(); it_str ++)
    *output << "\"" << *it_str << "\" ";
  *output << endl;
  *output << "\tnode [ shape = plaintext ];" << endl;
  *output << "\t\"\"" << endl;
  *output << "\tnode [ shape = circle ];" << endl;
  for (it_str = init.begin(); it_str != init.end(); it_str ++)
    *output << "\t\"\" -> \"" << *it_str << "\" " << endl;
  *output << endl;
  *output << trans.str();
  for (it_str = discrete.begin(); it_str != discrete.end(); it_str ++)
    *output << "\t\"" << *it_str << "\"; " << endl;
  *output << "}" << endl;
}

extern "C" int run(vector<Container> * containers, stringstream * output) {
  if (containers->size() != 1) {
    stringstream err;
    if (containers->size() > 1) {
      err << "Expecting one container to export, but I've got more.";
    } else if (containers->size() == 0) {
      err << "Expecting one container to export, but I haven't got any.";
    }
    PRINT_ERROR(err.str());
    return 1;
  }

  Container c =(*containers)[0];

  if (c.contains_automaton()) {
    export_automaton(c.getAutomaton(), output);
  } else {
    PRINT_ERROR("Container doesn't contain an automaton."
         << " Cannot export ");
    PRINT_ERROR("Exporting failed.");
    return -1;
  }
  return 0;
}

extern "C" const char * getName(void) {
  return "dot";
}

extern "C" const char * getInfo(void) {
  return "Export an automaton in DOT language.";
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
