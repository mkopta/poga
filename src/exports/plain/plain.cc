/* Copyright(c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files(the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
'plain' export module for poga system                             }}}1*/

#include <queue>
#include <sstream>
#include <set>
#include <vector>
#include <string>
#include "./poga.h"
#include "./container.h"
#include "./safestring.h"

using std::stringstream;
using std::set;
using std::string;
using std::vector;
using data_types::Automaton;
using data_types::Grammar;
using data_types::PlainRegex;
using data_types::automaton_ns::Transition;
using data_types::automaton_ns::State;
using data_types::grammar_ns::Rule;
using data_types::grammar_ns::Element;

int
export_automaton(const Automaton & a, stringstream * output) {
  stringstream              trans;
  string                    comment;
  set<string>               states;
  set<string>               symbols;  // alphabet
  set<string>               final;
  set<string>               init;
  set<Transition>           tr      = a.getTransitions();
  set<State>                st      = a.getDiscreteStates();
  set<Transition>::iterator it_tr;
  set<State>::iterator      it_st;
  set<string>::iterator     it_str;

  // first collect unused symbols
  symbols = a.getUnusedSymb();

  // then collect discrete states
  for (it_st = st.begin(); it_st != st.end(); it_st ++)
    states.insert(it_st->getName());

  // then add all used states and symbols
  for (it_tr = tr.begin(); it_tr != tr.end(); it_tr ++) {
    states.insert(it_tr->getSrc().getName());
    states.insert(it_tr->getDest().getName());
    symbols.insert(it_tr->getSymb());

    if (it_tr->getSrc().isFinal())
      final.insert(it_tr->getSrc().getName());
    if (it_tr->getDest().isFinal())
      final.insert(it_tr->getDest().getName());

    trans << "\t\"" << safestring(it_tr->getSrc().getName()) << "\"\t";
    trans << "\"" << safestring(it_tr->getSymb()) << "\"";
    trans << "=>\t";
    trans << "\"" << safestring(it_tr->getDest().getName()) << "\"";
    trans << endl;
  }

  // find inits
  set<State> inits = a.getInit();
  for (it_st = inits.begin(); it_st != inits.end(); it_st ++) {
    init.insert(it_st->getName());
  }

  // for the end get comment
  comment = safestring(a.getComment());

  // mix the soup..
  *output << "type { \"automaton\" }" << endl << endl;

  if (!comment.empty()) {
    *output << "comment" << endl << "{" << endl << "\"" << comment;
      *output << "\" " << endl;
    *output << "}" << endl << endl;
  }

  *output << "states" << endl << "{" << endl;
  for (it_str = states.begin(); it_str != states.end(); it_str ++)
    *output << "\t\"" << safestring(*it_str) << "\"" << endl;
  *output << "}" << endl << endl;

  *output << "alphabet" << endl << "{" << endl;
  for (it_str = symbols.begin(); it_str != symbols.end(); it_str ++)
    *output << "\t\"" << safestring(*it_str) << "\"" << endl;
  *output << "}" << endl << endl;

  *output << "transitions" << endl << "{" << endl;
  *output << trans.str();
  *output << "}" << endl << endl;

  *output << "final" << endl << "{" << endl;
  for (it_str = final.begin(); it_str != final.end(); it_str ++)
    *output << "\t\"" << safestring(*it_str) << "\"" << endl;
  *output << "}" << endl << endl;

  *output << "init" << endl << "{" << endl;
  for (it_str = init.begin(); it_str != init.end(); it_str ++)
    *output << "\t\"" << safestring(*it_str) << "\"" << endl;
  *output << "}" << endl << endl;

  return 0;
}

int
export_grammar(const Grammar & g, stringstream * output) {
  stringstream             rules;
  string                   comment;
  string                   init         = g.getInit().getName();
  set<string>              terminals;
  set<string>              nonterminals;
  vector<Rule>             ru;
  set<string>::iterator    it_str;
  bool                     init_in_rules= false;

  g.getRules(&ru);


  // going thru all rules of grammar
  for (unsigned int i = 0; i < ru.size(); i ++) {
    // disassemble rule
    Element         lside;
    vector<Element> rside;

    ru[i].getLeftside(&lside);
    ru[i].getRightside(&rside);

    // use the parts

    // leftside must be always nonterminal
    nonterminals.insert(lside.getName());

    rules << "\t\"" << lside.getName() << " => ";
    if (lside.isStartSymb()) init_in_rules = true;

    for (unsigned int y = 0; y < rside.size(); y++) {
      if (rside[y].isTerminal())
        terminals.insert(rside[y].getName());
      else
        nonterminals.insert(rside[y].getName());

      if (rside[y].isStartSymb()) init_in_rules = true;

      rules << "\"" << rside[y].getName() << "\" ";
    }

    rules << " ." << endl;
  }

  if (!init_in_rules)
    nonterminals.insert(init);

  comment = g.getComment();

  // completing
  *output << "type { \"grammar\" }" << endl << endl;

  *output << "terminals" << endl << "{" << endl;
  for (it_str = terminals.begin(); it_str != terminals.end(); it_str++)
    *output << "\t\"" << *it_str << "\"" << endl;
  *output << "}" << endl << endl;

  *output << "nonterminals" << endl << "{" << endl;
  for (it_str = nonterminals.begin(); it_str != nonterminals.end(); it_str++)
    *output << "\t\"" << *it_str << "\"" << endl;
  *output << "}" << endl << endl;

  *output << "init { \"" << init << "\" }" << endl << endl;

  *output << "rules" << endl << "{" << endl;
  *output << rules.str() << endl;
  *output << "}" << endl << endl;

  return 0;
}

int
export_plainregex(const PlainRegex & pr, stringstream * output) {
  *output << "type { \"plainregex\" }" << endl << endl;

  *output << "comment" << endl << "{" << endl;
  *output << "\t\"" << pr.getComment() << "\"" << endl;
  *output << "}" << endl << endl;

  *output << "plainregex" << endl << "{" << endl;
  *output << "\t\"" << pr.toString() << "\"" << endl;
  *output << "}" << endl << endl;

  return 0;
}

extern "C" int run(vector<Container> * containers, stringstream * output) {
  if (containers->size() != 1) {
    stringstream err;
    if (containers->size() > 1) {
      err << "Expecting one container to export, but I've got two.";
    } else if (containers->size() == 0) {
      err << "Expecting one container to export, but I haven't got any.";
    }
    PRINT_ERROR(err.str());
    return 1;
  }

  Container c =(*containers)[0];
  int retval = 0;

  if (c.contains_automaton()) {
    retval = export_automaton(c.getAutomaton(), output);
  } else if (c.contains_grammar()) {
    retval = export_grammar(c.getGrammar(), output);
  } else if (c.contains_plainregex()) {
    retval = export_plainregex(c.getPlainRegex(), output);
  } else {
    PRINT_ERROR("Container contains unknown data structure."
         << " Cannot export ");
    retval = 1;
  }

  if (retval != 0) {
    PRINT_ERROR("Exporting failed.");
    return 1;
  }

  return 0;
}

extern "C" const char * getName(void) {
  return("plain");
}

extern "C" const char * getInfo(void) {
  return("Export as plain poga data file.");
}

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
