/* Copyright (c) <2009> <Martin Kopta, martin@kopta.eu>             {{{1

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************
Header file describing inner data types of container class        }}}1*/

#ifndef SRC_DATA_TYPES_H_
#define SRC_DATA_TYPES_H_

#include "./typedefs.h"

namespace data_types {
  namespace grammar_ns {
    class Element {
      private:
        bool is_terminal;
        bool is_startSymb;
        STR name;
      public:
        Element(STR name, bool isTerminal, bool isStartSymb);
        Element();
        virtual ~Element();
        virtual void setIsTerminal(bool isTerminal);
        virtual void setIsStartSymb(bool isStartSymb);
        virtual bool isTerminal() const;
        virtual bool isStartSymb() const;
        virtual void setName(STR name);
        virtual STR  getName() const;

        virtual bool operator==(const Element &el) const;
        virtual bool operator!=(const Element &el) const;
        virtual bool operator<(const Element &el) const;
        virtual Element operator=(const Element &el);
    };

    class Rule {
      private:
        UI mark;
        EL leftside;
        VEC_EL rightside;
      public:
        Rule();
        explicit Rule(EL rightside);
        Rule(EL leftside, VEC_EL rightside);
        Rule(EL leftside, EL rightside);
        virtual ~Rule();

        virtual int getNext(EL *el);
        virtual int setMark(UI mark);
        virtual void resetMark();
        virtual int getFirst(EL *el) const;
        virtual int getLeftside(EL *el) const;
        virtual int getRightside(VEC_EL *elements) const;
        virtual void setLeftside(EL el);
        virtual void setRightside(VEC_EL elements);
        virtual void appendRightside(EL el);
        virtual bool rightsideIsEmpty() const;
        virtual bool leftsideIsStartSymb() const;
        virtual UI getRightsideLenght() const;
        virtual int getElementAt(UI position, EL *el) const;
        virtual int changeElementAt(UI position, EL newEl);

        virtual bool operator==(const Rule &el) const;
        virtual bool operator!=(const Rule &el) const;
        virtual bool operator<(const Rule &el) const;
        virtual Rule operator=(const Rule &el);
    };
  }

  namespace automaton_ns {
    class State {
      private:
        bool is_final;
        bool is_initial;
        STR name;
      public:
        State(STR name, bool is_final, bool is_initial);
        State(const State &st);
        virtual ~State();

        virtual bool isFinal() const;
        virtual bool isInitial() const;
        virtual STR getName() const;

        virtual void setIsInit(bool is);
        virtual void setIsFinal(bool is);
        virtual void setName(STR name);

        virtual bool operator!=(const State &st) const;
        virtual bool operator==(const State &st) const;
        virtual bool operator<(const State &st) const;
        virtual State operator=(const State &copy);
    };

    class HalfTransition {
      private:
      public:
        ST  dest;
        STR symb;

        HalfTransition(STR symb, ST dest);
        virtual ~HalfTransition();

        virtual ST getDest() const;
        virtual STR  getSymb() const;
        virtual bool isDestFinal() const;
        virtual void setDest(ST state);
        virtual void setSymb(STR symb);
        virtual bool symbEquals(STR *symb) const;
        virtual bool destEquals(ST  *state) const;

        virtual bool operator<(const HalfTransition &ht) const;
        virtual bool operator==(const HalfTransition &ht) const;
        virtual bool operator!=(const HalfTransition &ht) const;
    };


    class Transition {
      private:
      public:
        ST src;
        STR symb;
        ST dest;

        Transition(ST src, STR symb, ST dest);
        virtual ~Transition();

        virtual ST getSrc() const;
        virtual STR getSymb() const;
        virtual ST getDest() const;

        virtual bool isDestFinal() const;
        virtual bool isSrcInit() const;
        virtual bool symbEquals(STR *symbol);
        virtual bool srcEquals(ST *state);
        virtual bool destEquals(ST *state);
        virtual bool equals(TR *trans);

        virtual bool operator<(const Transition &tr) const;
        virtual bool operator==(const Transition &tr) const;
        virtual bool operator!=(const Transition &tr) const;
    };
  }

  class Automaton {
    private:
      SET_ST discreteStates;
      SET_STR unusedSymbols;
      SET_TR transitions;
      STR comment;
      SET_ST init;
    public:
      Automaton();
      Automaton(const SET_ST &states, const SET_STR &symbols);
      virtual ~Automaton();

      virtual void addTransition(const ST src, const STR symbol, const ST dest);
      virtual void addTransition(const TR &trans);

      virtual void addState(const ST &state);
      virtual void addSymbol(const STR &symbol);

      virtual void setComment(const STR comment);
      virtual STR  getComment() const;

      virtual SET_STR getUnusedSymb() const;
      virtual SET_ST getDiscreteStates() const;
      virtual SET_TR getTransitions() const;
      virtual SET_STR getAlphabet() const;
      virtual SET_STR getSymbols() const;
      virtual SET_ST getStates() const;

      virtual void setInit(const SET_ST &init);
      virtual SET_ST getInit() const;

      virtual SET_HT expand(const ST &src) const;
      virtual SET_ST cross(const ST &src, const STR &symb) const;
      virtual void clear();
  };


  class Grammar {
    private:
      VEC_RU rules;
      STR comment;
      EL init;

    public:
      explicit Grammar();
      explicit Grammar(VEC_RU rules);
      explicit Grammar(RU rule);
      virtual ~Grammar();

      virtual void addRule(RU rule);
      virtual void addRules(VEC_RU rules);
      virtual int getRule(UI * rule_num, RU *dest) const;
      virtual int getRules(VEC_RU *rules) const;
      virtual int changeRule(UI *rule_num, RU newrule);
      virtual UI lines() const;
      virtual int getRightside(UI *ruleNo, VEC_EL *rside) const;
      virtual VEC_UI getRulesNum(EL *nonterminal) const;
      virtual void setInit(EL init);
      virtual EL getInit() const;

      virtual void setComment(STR comment);
      virtual STR getComment() const;

      virtual void clear();
      /*
        suggestions for functions

      int getFirst(UI rule_num, VEC_EL *terminals);
      int getFIRST(std::multimap<EL,VEC_EL > * FIRST);
      int getFOLLOW(..);
      ..
         zjistit konflikty
         prevod na LL pomoci nasledujicich fci
         eliminace prime leve rekurze
         leva faktorizace
         rohova substituce
         pohlceni terminalu
       */
  };

  namespace regex_ns {
    /* nothing here yet */
  }

  class PlainRegex {
    private:
      UI mark;
      VEC_CH expression;
      STR comment;
    public:
      explicit PlainRegex();
      explicit PlainRegex(VEC_CH expression);
      explicit PlainRegex(STR expression);
      virtual ~PlainRegex();

      virtual void append(char character);
      virtual void setExpression(VEC_CH expression);
      virtual void setExpression(STR expression);
      virtual int setMark(UI position);
      virtual int length() const;
      virtual char getNextChar();
      virtual STR toString() const;
      virtual VEC_CH getExpression() const;

      virtual void setComment(STR comment);
      virtual STR getComment() const;

      virtual char operator[](UI position) const;
  };


  /*
  class UnixRegex { .. };
  class PerlRegex { .. };
  class MathRegex { .. };
  */
}

#endif  // SRC_DATA_TYPES_H_

/* vim: set ts=2 sw=2 expandtab fdm=marker: */
/* EOF */
